# SemanticRelComposition #

This is the SemanticRelComposition project. Out of an arbitrary knowledge base the SemanticRelComposition is able to derive compositions of semantic relations in the form of first-order logic rules. 
In addition the rules are assigned with a weight representing the quality and the evidence of the rule. This is possible for every knowledge base 
with facts having the following format: (s ,p, o), with s being the corresponding subject of the fact, o the object of the statement and p the 
predicate linking theese two entities with a semantic relationship. Currently the project supports all knowledge bases with the previously mentioned format with queries in SQL, however,
knowledge bases in RDF are also supported with queries in SPARQL. It also contains a specific setting for DBPedia and also FreeBase with MQL queries.

### How do I get set up? ###

You can easily make tests with the already installed knowledge bases like DBPedia with the public SPARQL endpoint or your own local endpoint.
For making experiments with Yago, download the corresponding .tsv/.csv ([Yago](http://www.mpi-inf.mpg.de/departments/databases-and-information-systems/research/yago-naga/yago/downloads/) files (yagoFacts package in the CORE theme) and set up a local PostgresSQL database. [Look up how to set up a Postgres database here](https://wiki.postgresql.org/wiki/Detailed_installation_guides). In general the set up is easily made by the attached config.xml file. All the various important parameters for specific experiments can be set here. If you want to use your own knowledge base then follow the subsequent instructions:

### Install PostgreSQL Database ###

First of all you need to get a PostgreSQL server running. [Follow this instructions](https://wiki.postgresql.org/wiki/Detailed_installation_guides). After finishing this step make sure to provide all important database settings like database name, host, port, username and password within the config.xml.

### Create knowledge schema ###

After the installation of the PostgreSQL server and the basic configuration, select your database and create a new schema named 'knowledge'. Within this schema create a table named 'KnowledgeBase' with the following columns/attributes:

* Name: s Type: character varying or text
* Name: p Type: character varying or text
* Name: o Type: character varying or text

It is important to have exactly this schema with this table contained. If your facts in your knowledge base have a additional attributes make sure to copy only the required attributes into the just generated table "knowledge"."KnowledgeBase" table.

### Run SemanticRelComposition  ###

Specify all parameters for yourt experiments in the config.xml. A detailed description of each parameter is provided in the config.xml. After setting up the knowledge base and all parameters, run the SemanticRelComposition. The SemanticRelComposition  generates all files in the corresponding folder structure like the extracted rule sets with weight thresholds, a proper representation of the rules, the to-be-annoated file and statistic files containing some basic statistics of the results. For annotating the to-be-annotated files run the Annotator. This generates annotated files and calculates the precision values of the annotated rule sets.

### Example Knowledge Base ###

For testing purposes we provide an example knowledge base consisting only of a few facts in the **examplekb.csv** file in the **example folder** and a list of properties **examplekb.prop** for this knowledge base. In the Example folder all genereated files of the example knowledge base are contained for 2-Hop rules. If you want to do some tests with this knowledge base, just import the examplekb.csv as described previously into a PostgreSQl database and run the SemanticRelComposition to see how the files are generated. The configuration file **config.xml** is initially set up for this example knowledge base.

### Experimental Evaluation Resources ###

This repository contains also the resources of the experimental evaluation of the SemanticRelComposition implementation for DBPedia and YAGO and 2-Hop and 3-Hop rules, respectively. In addition to the rule sets, we provide the complete to-be-annotated and annotated files and statistic files. These resources are contained in the results folder.
package de.uni_mannheim.dws.semanticrelcomposition.annotation;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;

import de.uni_mannheim.dws.semanticrelcomposition.pathsearch.Finder;
import de.uni_mannheim.dws.semanticrelcomposition.pathsearch.SemanticRelComposition;
import de.uni_mannheim.dws.semanticrelcomposition.pathsearch.Path;
import de.uni_mannheim.dws.semanticrelcomposition.rulelearning.Rule;
import de.uni_mannheim.dws.semanticrelcomposition.utilities.DBService;
import de.uni_mannheim.dws.semanticrelcomposition.utilities.Encoder;
import de.uni_mannheim.dws.semanticrelcomposition.utilities.QueryService;
import de.uni_mannheim.dws.semanticrelcomposition.utilities.ResultOffsetException;
import de.uni_mannheim.dws.semanticrelcomposition.utilities.StorageService;
/**
 * The ACreator is responsible for creating an to-be-annotated file with
 * the corresponding number of rules and for each rule the specified number of samples
 * @author Kristian Kolthoff
 *
 */
public class ACreator {

	/**
	 * The suffix for the to-be-annotated file
	 */
	public static final String SUFFIX = ".tba";
	public static final String SEPERATOR = "=";
	private StorageService sService;
	private DBService dbService;
	private QueryService qService;
	private Encoder enc;
	private String dest;
	private String file;
	private int ruleCount;
	
	public static Logger log = Logger.getLogger(ACreator.class.getCanonicalName());
	
	public ACreator() {
		this.sService = new StorageService();
		try {
			this.dbService = new DBService(SemanticRelComposition.getDatabase(), SemanticRelComposition.getUser(), SemanticRelComposition.getPassword());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		this.qService = new QueryService(SemanticRelComposition.getSparqlService());
		this.enc = new Encoder();
		this.dest = SemanticRelComposition.getRuleDest();
		this.file = SemanticRelComposition.getPrefix();
		this.ruleCount = 0;
	}

	/**
	 * Creates an tba file for the corresponding rule sets
	 */
	public void create() {
		for (int i = 0; i < SemanticRelComposition.getWeightThresh().length; i++) {
			try {
				sService.initReader(dest + file + "-" +  SemanticRelComposition.getWeightThresh()[i], 0);
				newChunk(SemanticRelComposition.getWeightThresh()[i]);
				sService.hasNextResult();
				while(sService.hasNextResult()) {
					if(ruleCount >= SemanticRelComposition.getNumOfAnnotationRules()) {
						break;
					}
					Rule tr = sService.nextRuleTSV();
					findSamples(tr);
					ruleCount++;
				}
				} catch (FileNotFoundException | ResultOffsetException e) {
					e.printStackTrace();
				}
		}
	}
	
	/**
	 * Finds samples for a TRule
	 * @param tr - the TRule we want to find samples for
	 */
	public void findSamples(Rule tr) {
		try{
		if(SemanticRelComposition.getFinderType() == Finder.SQL) {
			sampleDB(tr);
		} else if(SemanticRelComposition.getFinderType() == Finder.SPARQL) {
			sampleQuerySPARQL(tr);
		}
		}catch(Exception e) {
			
		}
	}
	
	/**
	 * Extracts instances for a TRule with SQL and builds up the actual tba file
	 * @param tr
	 */
	public void sampleDB(Rule tr) {
		log.info("[Writing] : " + ruleCount + " " +tr.toString());
		String rule = enc.decodeTRuleYago(tr);
		dbService.sampleQueryDB(tr, SemanticRelComposition.getNumOfAnnotationSamples());
		ArrayList<Path> tps = dbService.getResults();
		sService.buffer(rule);
		sService.write();
		for (int i = 0; i < tps.size(); i++) {
			for (int j = 0; j < tps.get(i).getPath().size(); j++) {
				if(j == tps.get(i).getPath().size()-3) {
					sService.buffer(SEPERATOR);
				}
				sService.buffer(enc.removeYagoSyn(tps.get(i).getPath().get(j)) + "\t");
			}
			sService.write();
		}
	}
	
	/**
	 * Extracts instances for a TRule with SPARQL and builds up the actual tba file.
	 * @param tr
	 */
	public void sampleQuerySPARQL(Rule tr) {
		log.info("[Writing] : " + ruleCount +" " + tr.toString());
		ResultSet rs = qService.sampleQuery(tr, SemanticRelComposition.getNumOfAnnotationSamples());
		String rule = enc.decodeTRuleDBPedia(tr, false);
		sService.buffer(rule);
		sService.write();
		while(rs.hasNext()) {
			QuerySolution qs = rs.next();
			for (int i = 0; i < SemanticRelComposition.getHops(); i++) {
				sService.buffer(enc.removeDBPediaSynCompl(qs.getResource("e"+i).getURI()) + "\t");
				sService.buffer(enc.removeDBPediaSynCompl(tr.getPropsBody().get(i).getName()) + "\t");
			}
			sService.buffer(enc.removeDBPediaSynCompl(qs.getResource("e"+SemanticRelComposition.getHops()).getURI()));
			sService.buffer(SEPERATOR);
			sService.buffer(enc.removeDBPediaSynCompl(qs.getResource("e0").getURI()) + "\t" + enc.removeDBPediaSynCompl(tr.getPropHead().getName()) + "\t"
					+ enc.removeDBPediaSynCompl(qs.getResource("e"+SemanticRelComposition.getHops()).getURI()));
			sService.write();
		}
	}
	
	
	/**
	 * Creates a new chunk
	 * @param weightThresh - the rule weights threshold for this chunk
	 */
	public void newChunk(int weightThresh) {
		sService.createFile(SemanticRelComposition.getTbaDest()+SemanticRelComposition.getPrefix()+"-"+weightThresh+SUFFIX);
	}

}
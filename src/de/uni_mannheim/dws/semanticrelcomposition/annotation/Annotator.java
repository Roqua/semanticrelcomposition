package de.uni_mannheim.dws.semanticrelcomposition.annotation;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import de.uni_mannheim.dws.semanticrelcomposition.pathsearch.SemanticRelComposition;
import de.uni_mannheim.dws.semanticrelcomposition.utilities.ResultOffsetException;
import de.uni_mannheim.dws.semanticrelcomposition.utilities.StorageService;

/**
 * Loads .tba file and displays it to the annotator in the console and writes back the results of the annotation process.
 *@author Kristian Kolthoff
 */
public class Annotator {

	public static final String SUFFIX = ".a";
	public static final String TRUE = "y";
	public static final String FALSE = "n";
	private StorageService sService;
	private Scanner sc;
	private int count = 0;
	private String currFile;
	
	public Annotator() {
		this.sService = new StorageService();
		this.sc = new Scanner(System.in);
	}
	
	/**
	 * Coordinates the annotation process
	 */
	public void annotate(String path) {
		config(path);
		currFile = path;
		System.out.println("Welcome! Very cool that you are helping me with annotating some rules, thanks a lot in advance! :) "
				+ "\n"+ "You will see a general transitive rule. Afterwards 10 samples fullfilling this rule will be presented to you."
				+ "\n"	+ "Your task will be to decide wether the sample instances hold and subsequently the rule or if the rule is meaningful,"
				+ "\n"	+ "in terms of if the rule holds generally."
						+ "\n"+ "Should we start now? :)");
		sc.next();
		ArrayList<String> results = new ArrayList<>();
		String currRule = "";
		if(sService.hasNextResult()) {
			currRule = sService.nextResult();			
		}
		while(sService.hasNextResult()) {
			String rule = sService.nextResult();
			if(rule.contains("=")) {
				results.add(rule);
			} else {
				prettyRulePrint(currRule);
				prettyResultPrint(results);
				analyze(rule);
				currRule = rule;
				results.clear();
				count++;
			}
		}
		System.out.println("Thanks for annotating this file! Much appreciated!");
	}
	
	/**
	 * Prints the rule instances pretty on the console
	 * @param results - the instances to display
	 */
	public void prettyResultPrint(ArrayList<String> results) {
		for (int i = 0; i < results.size(); i++) {
			String[] ruleInst = results.get(i).split(ACreator.SEPERATOR);
			System.out.print(i + ".) ");
			String[] body = ruleInst[0].split("\t");
			String head[] = ruleInst[1].split("\t");
			for (int j = 0; j < body.length; j++) {
				System.out.printf("%-30.25s", body[j]);
			}
			System.out.print(" || ");
			for (int j = 0; j < head.length; j++) {
				System.out.printf("%-20.15s", head[j]);
			}
			System.out.println();
		}
	}
	
	/**
	 * Prints the rule pretty on the console
	 * @param rule - the rule to display
	 */
	public void prettyRulePrint(String rule) {
		for (int i = 0; i < rule.length()+20; i++) {
			System.out.print("-");	
		}
		System.out.println();
		System.out.print("|");
		for (int i = 0; i < rule.length()+18; i++) {
			System.out.print(" ");
		}
		System.out.println("|");
		System.out.print("|");
		for (int i = 0; i < 10; i++) {
			System.out.print(" ");
		}
		System.out.print(rule);
		for (int i = 0; i < 8; i++) {
			System.out.print(" ");
		}
		System.out.println("|");
		System.out.print("|");
		for (int i = 0; i < rule.length()+18; i++) {
			System.out.print(" ");
		}
		System.out.println("|");
		for (int i = 0; i < rule.length()+20; i++) {
			System.out.print("-");	
		}
		System.out.println();
	}
	
	/**
	 * Captures the user input
	 * @param rule
	 */
	public void analyze(String rule) {
		System.out.println();
		System.out.println("["+count +"%] What do you think about this rule? Does it hold and is it meaningful? If so please type \"y\", otherwise type \"n\": ");
		String s;
		while(true) {
			System.out.println("Please type y if the rule holds, otherwise type n:");
			s = sc.next();
			if(s.equals(TRUE) || s.equals(FALSE)) {
				break;
			}
		}
		sService.buffer(rule);
		if(s.equals(TRUE)) {
			sService.buffer("\t" + "true");
		} else if(s.equals(FALSE)) {
			sService.buffer("\t" + "false");
			System.out.println("I already thought that this rule is not too good. Thanks for giving me confidence :) ");
		}
		sService.write();
	}
	
	/**
	 * Basic config of the annotator.
	 */
	public void config(String path) {
		try {
			sService.initReader(new File("").getAbsolutePath() + path, 0);
			String[] s = path.replace('/', '#').split("#");
			System.out.println();
			String prefix = s[s.length-1].replace('.', '#').split("#")[0];
			sService.createFile(SemanticRelComposition.getTbaDest()+prefix+SUFFIX);
		} catch (FileNotFoundException | ResultOffsetException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * This starts the Annotator to annotate top-k rule sets.
	 * The current file path of the .tba file needs to be specified here
	 * @param args
	 */
	public static void main(String[] args) {
		SemanticRelComposition trf = new SemanticRelComposition();
		trf.basicConfig();
		Annotator a = new Annotator();
		a.annotate("/results/Example/2-Hop/Annotation/ToBeAnnotated/example-1.tba");
		int[] k = new int[11];
		k[0] = 5;
		for (int i = 1; i < k.length; i++) {
			k[i] = i*10;
		}
		AParser ap = new AParser(k, "/results/Example/2-Hop/Annotation/ToBeAnnotated/example-1.a");
		ap.initRuleVals();
		ap.topKVals();
	}

	public String getCurrFile() {
		return currFile;
	}

	public void setCurrFile(String currFile) {
		this.currFile = currFile;
	}
}

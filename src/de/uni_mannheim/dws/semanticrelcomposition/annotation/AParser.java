package de.uni_mannheim.dws.semanticrelcomposition.annotation;

import java.io.File;
import java.io.FileNotFoundException;

import de.uni_mannheim.dws.semanticrelcomposition.pathsearch.SemanticRelComposition;
import de.uni_mannheim.dws.semanticrelcomposition.utilities.ResultOffsetException;
import de.uni_mannheim.dws.semanticrelcomposition.utilities.StorageService;
/**
 * Parses the annotated file and calculates p@k values for various ks
 * @author Kristian Kolthoff
 *
 */
public class AParser {

	private StorageService sService;
	private int[] kPoints;
	private boolean[] ruleVals;
	private String file;
	
	public AParser(int[] kPoints, String file) {
		this.sService = new StorageService();
		this.kPoints = kPoints;
		this.ruleVals = new boolean[100];
		this.file = new File("").getAbsolutePath() + file;
	}
	
	/**
	 * Reads in the 
	 */
	public void initRuleVals() {
		try {
			sService.initReader(file, 0);
			int count = 0;
			while(sService.hasNextResult()) {
				String[] s = sService.nextResult().split("\t");
				ruleVals[count] = Boolean.valueOf(s[s.length-1]);
				count++;
			}
		} catch (FileNotFoundException | ResultOffsetException e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Precision for top-k
	 */
	public void topKVals() {
		sService.createFile(SemanticRelComposition.getRuleDest()+SemanticRelComposition.getPrefix() + ".stats");
		for (int i = 0; i < kPoints.length; i++) {
			float p = precisionAtK(kPoints[i]);
			String res = "p@"+kPoints[i] + ": " + p ;
			System.out.println(res);
			sService.buffer(res);
			sService.write();
		}
	}
	
	/**
	 * p@k
	 * @param k
	 * @return
	 */
	public float precisionAtK(int k) {
		int relevant = 0;
		for (int i = 0; i < k; i++) {
			if(ruleVals[i]) {
				relevant++;
			}
		}
		return (relevant / (float) k);
	}

	public int[] getkPoints() {
		return kPoints;
	}

	public void setkPoints(int[] kPoints) {
		this.kPoints = kPoints;
	}

	public boolean[] getRuleVals() {
		return ruleVals;
	}

	public void setRuleVals(boolean[] ruleVals) {
		this.ruleVals = ruleVals;
	}
	
}

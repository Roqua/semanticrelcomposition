package de.uni_mannheim.dws.semanticrelcomposition.pathsearch;

import java.util.ArrayList;
/**
 * A TransitivePath represents a result of the TPathFinder process.
 * It consists of an entity e1, an entity e2 and a n-hop path of properties
 * from e1 to e2. Additionally it also contains a direct property and maybe
 * an inverse property from e1 to e2.
 * @author Kristian Kolthoff
 */
public class Path {

	private String e1;
	private String e2;
	private ArrayList<String> tPath;
	private String head;
	private boolean isInverse;
	
	public Path() {
		this.tPath = new ArrayList<>();
	}
	
	public Path(String e1) {
		this.e1 = e1;
		this.tPath = new ArrayList<>();
	}
	public Path(String e1, String e2) {
		this.e1 = e1;
		this.e2 = e2;
		this.tPath = new ArrayList<>();
	}
	
	public Path(String e1, String e2, String head) {
		this.e1 = e1;
		this.e2 = e2;
		this.head = head;
		this.tPath = new ArrayList<>();
	}
	
	/**
	 * Adds another property to the path
	 * @param t the property as an URI
	 */
	public boolean addStep(String t) {
		return tPath.add(t);
	}

	public ArrayList<String> getPath() {
		return tPath;
	}

	public void setPath(ArrayList<String> path) {
		this.tPath = path;
	}

	public String getE1() {
		return e1;
	}

	public void setE1(String e1) {
		this.e1 = e1;
	}

	public String getE2() {
		return e2;
	}

	public void setE2(String e2) {
		this.e2 = e2;
	}


	@Override
	public String toString() {
		String tp = getE1() + "\t";
		for (int i = 0; i < getPath().size(); i++) {
			tp += getPath().get(i) + "\t";
		}
		tp += getE2() + "\t" + getHead() + "\t" + isInverse();
		return tp;
	}
	public String getHead() {
		return head;
	}
	public void setHead(String head) {
		this.head = head;
	}
	
	public void setHeadAndDir(String head, boolean isInverse) {
		this.head = head;
		this.isInverse = isInverse;
	}
	
	public boolean isInverse() {
		return isInverse;
	}
	public void setInverse(boolean isInverse) {
		this.isInverse = isInverse;
	}
	
	
}
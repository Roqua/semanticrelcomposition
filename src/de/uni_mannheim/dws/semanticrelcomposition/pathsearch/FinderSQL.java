package de.uni_mannheim.dws.semanticrelcomposition.pathsearch;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.SQLDialect;
import org.jooq.SelectQuery;
import org.jooq.impl.DSL;

import static de.uni_mannheim.dws.semanticrelcomposition.knowledge.Tables.*;
import static org.jooq.impl.DSL.*;
import de.uni_mannheim.dws.semanticrelcomposition.knowledge.tables.Knowledgebase;
import de.uni_mannheim.dws.semanticrelcomposition.rulelearning.Property;
import de.uni_mannheim.dws.semanticrelcomposition.rulelearning.Rule;
import de.uni_mannheim.dws.semanticrelcomposition.utilities.DBService;
import de.uni_mannheim.dws.semanticrelcomposition.utilities.StorageService;
/**
 * The TFinderSQL implements the Finder interface for common relational knowledge bases queried by SQL.
 * Basically this class is responsible for constructing different SQL queries and processing the results.
 * Important to notice: This implementation needs a schema called "knowledge" and within this schema a table called
 * "KnowledgeBase" with (text s, text p, text o) attributes. This implementation uses an JDBC PostgreSQL driver.
 * @author Kristian Kolthoff
 */
public class FinderSQL implements Finder{
	
	private StorageService sService;
	private DBService dbService;
	private PathFinder tpFinder;
	
	private DSLContext ctx;
	/**
	 * The current select query
	 */
	private SelectQuery<Record> query;
	/**
	 * The current basic result record
	 */
	private Result<Record> rs;
	/**
	 * Several "knowledge"."KnowledgeBase" tables aliases
	 */
	private ArrayList<Knowledgebase> kbTables;
	/**
	 * The current rsList for the direct abstract query
	 */
	private ArrayList<List<String>> rsList;
	/**
	 * Simple list for concrete queries containing only heads of a body query
	 */
	private List<String> simpleList;
	/**
	 * Contains the correspoding counts
	 */
	private List<Integer> countList;
	/**
	 * Field for referring count value in result set
	 */
	private Field<Integer> f;
	
	public static Logger log = Logger.getLogger(FinderSQL.class.getCanonicalName());
	
	public FinderSQL(PathFinder tpFinder) throws SQLException {
		this.tpFinder = tpFinder;
		this.sService = new StorageService();
		this.dbService = new DBService(SemanticRelComposition.getDatabase(), SemanticRelComposition.getUser(), SemanticRelComposition.getPassword());
		dbService.getCon().setReadOnly(true);
		this.ctx = DSL.using(dbService.getCon(), SQLDialect.POSTGRES);
		this.kbTables = new ArrayList<>();
		this.rsList = new ArrayList<>();
		this.simpleList = new ArrayList<>();
		this.countList = new ArrayList<>();
	}

	@Override
	public void onQueryUpdate(boolean isInverse) {
		query = ctx.selectQuery();
		/**
		 * FROM
		 */
		for (int i = 0; i < SemanticRelComposition.getHops(); i++) {
			Knowledgebase kb = KNOWLEDGEBASE.as("kb" + i);			
			kbTables.add(kb);
			query.addFrom(kb);
		}
		Knowledgebase kbD = KNOWLEDGEBASE.as("kbD");
		/**
		 * SELECT
		 */
		query.addSelect(kbD.P);
		f = count(KNOWLEDGEBASE.as("kbD").P);
		query.addSelect(f);
		query.addFrom(kbD);
		kbTables.add(kbD);
		/**
		 * WHERE
		 */
		for (int i = 0; i < SemanticRelComposition.getHops()-1; i++) {
			Knowledgebase kbLeft = kbTables.get(i);
			Knowledgebase kbRight = kbTables.get(i + 1);
			query.addConditions(kbLeft.O.equal(kbRight.S));
			query.addConditions(kbLeft.P.eq(tpFinder.getCurrQueryProps()[i].getName()));
		}
		query.addConditions(kbTables.get(SemanticRelComposition.getHops()-1).P.eq(tpFinder.getCurrQueryProps()[SemanticRelComposition.getHops()-1].getName()));
		if(isInverse) {
			query.addConditions(kbTables.get(0).S.equal(kbD.O));
			query.addConditions(kbTables.get(SemanticRelComposition.getHops()-1).O.equal(kbD.S));
		} else {
			query.addConditions(kbTables.get(0).S.equal(kbD.S));
			query.addConditions(kbTables.get(SemanticRelComposition.getHops()-1).O.equal(kbD.O));
		}
		/**
		 * GROUP BY
		 */
		query.addGroupBy(KNOWLEDGEBASE.as("kbD").P);
		/**
		 * Filters for cycle avoidance
		 */
		for (int i = 0; i < SemanticRelComposition.getHops(); i++) {
			for (int j = i; j < SemanticRelComposition.getHops(); j++) {
				query.addConditions(kbTables.get(i).S.notEqual(kbTables.get(j).O));
			}
		}
	}


	@Override
	public void storeResults(boolean isInverse) {
		int rsCount = 0;
		log();
		ArrayList<Property> propsBody = new ArrayList<>();
		for (int j = 0; j < tpFinder.getCurrQueryProps().length; j++) {
			propsBody.add(tpFinder.getCurrQueryProps()[j]);
		}
		for (int i = 0; i < simpleList.size(); i++) {
			Property propHead = new Property(simpleList.get(i));
			Rule tr = new Rule(propsBody, propHead, isInverse);
			tr.setCount(countList.get(i));
			tpFinder.verifyChunk();
			rsCount++;
			sService.storeTRule(tr);
		}
		log.info(" [ID:] "+ tpFinder.getThreadID() +" ----------------->>> rs-size: " + rsCount);
		
	}
			
	@Override
	public void excecQuery() {
		rs = query.fetch();
		rsList.clear();
		if(SemanticRelComposition.isUseProperties()) {
			simpleList = rs.getValues(kbTables.get(SemanticRelComposition.getHops()).P);
		} else {
			for (int j = 0; j < SemanticRelComposition.getHops(); j++) {
				rsList.add(rs.getValues(kbTables.get(j).P));					
			}
			rsList.add(rs.getValues(kbTables.get(SemanticRelComposition.getHops()).P));
		}
		countList = rs.getValues(f);
	}
	
	/**		
	 * Logs the current query properies of the body of the rules.
	 */
	protected void log() {
		if(SemanticRelComposition.isUseProperties()) {
		StringBuilder sb = new StringBuilder("[Store:] " + " ID: " + tpFinder.getThreadID());
			for (int i = 0; i < tpFinder.getCurrQueryProps().length; i++) {
				sb.append(tpFinder.getCurrQueryProps()[i].getName() + " ");
			}
			log.info(sb.toString());
		} else {
			log.info("[Store:] " + " ID: " + tpFinder.getThreadID());
		}
	}
	

	@Override
	public boolean hasResults() {
		query.close();
		return !countList.isEmpty();
	}

	@Override
	public void createFile(String path) {
		sService.createFile(path);
	}

	@Override
	public boolean isCompatible() {
		for (int i = 0; i < tpFinder.getCurrQueryProps().length-1; i++) {
			Property p1 = tpFinder.getCurrQueryProps()[i];
			Property p2 = tpFinder.getCurrQueryProps()[i+1];
			String[] p2subDomain = p2.getSubClassHierachyDomain();
			/**
			 * Only if this condition is true, both properties have subclass hierachies for the domain
			 * and the range, respectively. Otherwise this pruning can not be ap
			 */
			if(p1.getSubClassHierachyDomain() != null && p2subDomain != null) {
				for (int j = 0; j < p2subDomain.length; j++) {
					if(p1.getRange().equals(p2subDomain[j])) {
						return true;
					}
				}
				return false;
			}
		}
		return true;
	}

	@Override
	public void closeConnection() {
		try {
			this.dbService.getCon().close();
		} catch (SQLException e) {
			e.printStackTrace();
			log.error("Could not close DB connection of thread ID: " + tpFinder.getThreadID());
		}
	}

	@Override
	public void directQuery(boolean isInverse) {
		query = ctx.selectQuery();
		/**
		 * FROM
		 */
		for (int i = 0; i < SemanticRelComposition.getHops(); i++) {
			Knowledgebase kb = KNOWLEDGEBASE.as("kb" + i);			
			kbTables.add(kb);
			query.addFrom(kb);
			query.addSelect(kb.P);
		}
		Knowledgebase kbD = KNOWLEDGEBASE.as("kbD");
		/**
		 * SELECT
		 */
		query.addSelect(kbD.P);
		f = count();
		query.addSelect(f);
		query.addFrom(kbD);
		kbTables.add(kbD);
		/**
		 * WHERE
		 */
		for (int i = 0; i < SemanticRelComposition.getHops()-1; i++) {
			Knowledgebase kbLeft = kbTables.get(i);
			Knowledgebase kbRight = kbTables.get(i + 1);
			query.addConditions(kbLeft.O.equal(kbRight.S));
		}
		if(isInverse) {
			query.addConditions(kbTables.get(0).S.equal(kbD.O));
			query.addConditions(kbTables.get(SemanticRelComposition.getHops()-1).O.equal(kbD.S));
		} else {
			query.addConditions(kbTables.get(0).S.equal(kbD.S));
			query.addConditions(kbTables.get(SemanticRelComposition.getHops()-1).O.equal(kbD.O));
		}
		/**
		 * GROUP BY
		 */
		for (int i = 0; i < SemanticRelComposition.getHops(); i++) {
			query.addGroupBy(kbTables.get(i).P);
		}
		query.addGroupBy(KNOWLEDGEBASE.as("kbD").P);
		/**
		 * Filters for cycle avoidance
		 */
		for (int i = 0; i < SemanticRelComposition.getHops(); i++) {
			for (int j = i; j < SemanticRelComposition.getHops(); j++) {
				query.addConditions(kbTables.get(i).S.notEqual(kbTables.get(j).O));
			}
		}
	}



	@Override
	public void storeDir(boolean isInverse) {
		int rsCount = 0;
		log();
		for (int i = 0; i < rsList.get(0).size(); i++) {
			ArrayList<Property> propsBody = new ArrayList<>();
			for (int j = 0; j < rsList.size()-1; j++) {
				propsBody.add(new Property(rsList.get(j).get(i)));
			}
				Property propHead = new Property(rsList.get(rsList.size()-1).get(i));
				Rule tr = new Rule(propsBody, propHead, isInverse);
				tr.setCount(countList.get(i));
				tpFinder.verifyChunk();
				rsCount++;
				sService.storeTRule(tr);
		log.info(" [ID:] "+ tpFinder.getThreadID() +" ----------------->>> rs-size: " + rsCount);
		}
	}
	
}

package de.uni_mannheim.dws.semanticrelcomposition.pathsearch;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import de.uni_mannheim.dws.semanticrelcomposition.annotation.ACreator;
import de.uni_mannheim.dws.semanticrelcomposition.rulelearning.Property;
import de.uni_mannheim.dws.semanticrelcomposition.rulelearning.RuleFinder;
/**
 * The TransRelFinder is the main class that coordinates the process of discovering transitive first-order logic rules in knowledge bases.
 * Therefore it first starts the raw extraction process with the TPathFinder, and afterwards extracts proper rules with all rule parameters computed.
 * Finally, the .tba files are generated.
 * @author Kristian Kolthoff 
 *
 */
public class SemanticRelComposition {

	/**
	 * Different public and local SPARQL endpoint service URIs
	 */
	public static final String SPARQL_SERVICE_PUBLIC = "http://dbpedia.org/sparql";
	public static final String SPARQL_SERVICE_LIVE = "http://live.dbpedia.org/sparql";
	public static final String SPARQL_SERVICE_LOCAL = "http://wifo5-32.informatik.uni-mannheim.de:8893/sparql";
	/**
	 * Timeout after SPARQL or SQL query error occured for example if the queries are to fast
	 * and exceed the endpoint queries per second limit
	 */
	public static final int TIMEOUT = 60000;
	
	
	public static Thread[] threads;
	/**
	 * Contains the number of queries done by each thread already
	 */
	public static int[] speedVec;
	/**
	 * Contains booleans whether the threads are done with their queries
	 */
	public static boolean[] threadsOver;
	
	private ArrayList<PathFinder> tFinder;
	private static ArrayList<String[]> propsRetry;
	
	/**
	 * All parameters that are specified in the config.xml file
	 */
	private static int hops;
	private static int finderType;
	private static boolean useProperties;
	private static String database;
	private static String user;
	private static String password;
	private static String sparqlService;
	private static String rawchunkDest;
	private static String ruleDest;
	private static String tbaDest;
	private static String propertiesDest;
	private static String prefix;
	private static int numOfThreads;
	private static int[] weightThresh;
	private static int numOfAnnotationRules;
	private static int numOfAnnotationSamples;
	private static ArrayList<Property> propertyList;
	
	public static Logger log = Logger.getLogger(SemanticRelComposition.class.getCanonicalName());
	
	public SemanticRelComposition() {
		this.tFinder = new ArrayList<>();
		propsRetry = new ArrayList<>();
	}
	
	public static void main(String[] args) {
		SemanticRelComposition trf = new SemanticRelComposition();
		trf.basicConfig();
		trf.runTPathFinder();
	}
	
	public static void runTRuleFinder() {
		RuleFinder trf = new RuleFinder();
		if(!isUseProperties()) {
			numOfThreads = 2;
		}
		trf.run();
	}
	
	/**
	 * Configure all important test settings like used knowledge base, SPARQL endpoint, output folders
	 * and output files, property list etc.
	 */
	public void basicConfig() {
		log.info("[Loading:] config.xml");
		parseConfigFile();
	}

	/**
	 * Reads in the parameter values defined in config.xml
	 */
	public void parseConfigFile() {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse(new File("").getAbsolutePath() + "/config.xml");
			Element rootElem = doc.getDocumentElement();
			NodeList root = rootElem.getChildNodes();
			hops = Integer.valueOf(getNextNode("NHops", root).getTextContent());
			finderType = Integer.valueOf(getNextNodeValue("KBType", root));
			if(getNextNodeValue("UseProperties", root).equals("true")) {
				useProperties = true;
			} else {
				useProperties = false;
			}
			sparqlService = getNextNodeValue("SPARQL", root);
			numOfThreads = Integer.valueOf(getNextNodeValue("NThreads", root));
			numOfAnnotationRules = Integer.valueOf(getNextNodeValue("NRules", root));
			numOfAnnotationSamples = Integer.valueOf(getNextNodeValue("NSamples", root));
			Node sql = getNextNode("SQLDatabase", root);
			NodeList nodesSQL = sql.getChildNodes();
			database = "jdbc:postgresql://" + getNextNodeValue("Host", nodesSQL) + ":" +
					getNextNodeValue("Port", nodesSQL) + "/" + getNextNodeValue("Name", nodesSQL);
			user = getNextNodeValue("User", nodesSQL);
			password = getNextNodeValue("Password", nodesSQL);
			Node files = getNextNode("Files", root);
			NodeList nodesFiles = files.getChildNodes();
			String src = new File("").getAbsolutePath() + getNextNodeValue("src", nodesFiles);
			rawchunkDest = src + getNextNodeValue("Raw", nodesFiles);
			ruleDest = src + getNextNodeValue("Rules", nodesFiles);
			tbaDest = src + getNextNodeValue("TBA", nodesFiles);
			propertiesDest = new File("").getAbsolutePath() + getNextNodeValue("Properties", nodesFiles);
			prefix = getNextNodeValue("Prefix", nodesFiles);
			Node weights = getNextNode("WeightThresh", root);
			NodeList weightVals = weights.getChildNodes();
			weightThresh = new int[(weightVals.getLength()-1)/2];
			int count = 0;
			for (int i = 0; i < weightVals.getLength()-1; i++) {
				if(weightVals.item(i).getNodeName().equals("Weight")) {
					weightThresh[count] = Integer.valueOf(weightVals.item(i).getTextContent());
					count++;
				}
			}
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * Returns the next node with the specified tag as the node name
	 * @param tag - the name of the node
	 * @param nodes - the node list containing the node with the tag
	 * @return the found node with the specified tag
	 */
	private Node getNextNode(String tag, NodeList nList) {
	    for (int i = 0; i < nList.getLength(); i++) {
	        Node n = nList.item(i);
	        if (n.getNodeName().equalsIgnoreCase(tag)) {
	            return n;
	        }
	    }
	 
	    return null;
	}
	 
	/**
	 * 
	 * @param tag - the name of the node
	 * @param nList - the nodes list
	 * @return the node value as a string
	 */
	private String getNextNodeValue(String tag, NodeList nList) {
	    for (int i = 0; i < nList.getLength(); i++) {
	        Node n = nList.item(i);
	        if (n.getNodeName().equalsIgnoreCase(tag)) {
	            NodeList cNodes = n.getChildNodes();
	            for (int j = 0; j < cNodes.getLength(); j++ ) {
	                Node d = cNodes.item(j);
	                if (d.getNodeType() == Node.TEXT_NODE)
	                    return d.getNodeValue();
	            }
	        }
	    }
	    return "";
	}
	
	/**
	 * Sets up the TPathFinder threads and starts the querying process
	 */
	public void runTPathFinder() {
		/**
		 * is useproperties flag is set to true, run the TPathFinder with numOfThreads threads
		 * Otherwise, start two threads for direct and inverse queries
		 */
		if(isUseProperties()) {
			log.info("[Loading:] " + propertiesDest);
			propertyList = PropertyFinder.loadLocalPropList(propertiesDest);
			threads = new Thread[numOfThreads];
			speedVec = new int[numOfThreads];
			threadsOver = new boolean[numOfThreads];
			int numOfProps = propertyList.size();
			int ratio = numOfProps / numOfThreads;
			for (int i = 0; i < numOfThreads; i++) {
				tFinder.add(new PathFinder(hops, i*ratio, (i*ratio) + ratio - 1, i+1, finderType));
				threads[i] = new Thread(tFinder.get(i), "TPathFinder" +(i+1));
				threads[i].start();
			}
		} else{
			/**
			 * Create two threads for query process. The first TPathFinder builds an abstract query for
			 * head predicates that connect head and tail entitiy directly. The second TPathFinder builds
			 * an abstract query for the head predicates that connect head and tail entity inversely.
			 */
			threadsOver = new boolean[2];
			PathFinder tpfDir = new PathFinder(hops, 0, 0, 1, finderType);
			PathFinder tpfInv = new PathFinder(hops, 0, 0, 2, finderType);
			tpfDir.run();
			tpfInv.run();
		}
		
	}
	
	/**
	 * Each thread which is over with querying the knowledge base
	 * calls this method. If all threads are done with extracting raw rules,
	 * the TRuleFinder started.
	 */
	public static void onThreadsOver() {
		for (int i = 0; i < threadsOver.length; i++) {
			if(!threadsOver[i]) {
				return;
			}
		}
		runTRuleFinder();
	}
	
	public static void onTRuleFinderOver() {
		ACreator ac = new ACreator();
		ac.create();
	}
	
	/**
	 * Computing the average number of queries a single thread has excecuted
	 * @return
	 */
	public static float compQueryAv() {
		int sum = 0;
		for (int i = 0; i < speedVec.length; i++) {
			sum += speedVec[i];
		}
		return Math.round((float)sum/speedVec.length);
	}

	public static ArrayList<String[]> getPropsRetry() {
		return propsRetry;
	}

	public static Thread[] getThreads() {
		return threads;
	}

	public static void setThreads(Thread[] threads) {
		SemanticRelComposition.threads = threads;
	}

	public static int getHops() {
		return hops;
	}

	public static void setHops(int hops) {
		SemanticRelComposition.hops = hops;
	}

	public static String getSparqlService() {
		return sparqlService;
	}

	public static void setSparqlService(String sparqlService) {
		SemanticRelComposition.sparqlService = sparqlService;
	}

	public static String getDatabase() {
		return database;
	}

	public static void setDatabase(String database) {
		SemanticRelComposition.database = database;
	}

	public static String getUser() {
		return user;
	}

	public static void setUser(String user) {
		SemanticRelComposition.user = user;
	}

	public static String getPassword() {
		return password;
	}

	public static void setPassword(String password) {
		SemanticRelComposition.password = password;
	}

	public static int getNumOfThreads() {
		return numOfThreads;
	}

	public static void setNumOfThreads(int numOfThreads) {
		SemanticRelComposition.numOfThreads = numOfThreads;
	}

	public static ArrayList<Property> getPropertyList() {
		return propertyList;
	}

	public static void setPropertyList(ArrayList<Property> propertyList) {
		SemanticRelComposition.propertyList = propertyList;
	}

	public static String getRawchunkDest() {
		return rawchunkDest;
	}

	public static void setRawchunkDest(String rawchunkDest) {
		SemanticRelComposition.rawchunkDest = rawchunkDest;
	}

	public static String getRuleDest() {
		return ruleDest;
	}

	public static void setRuleDest(String ruleDest) {
		SemanticRelComposition.ruleDest = ruleDest;
	}

	public static String getPrefix() {
		return prefix;
	}

	public static void setPrefix(String prefix) {
		SemanticRelComposition.prefix = prefix;
	}

	public static int getFinderType() {
		return finderType;
	}

	public static void setFinderType(int finderType) {
		SemanticRelComposition.finderType = finderType;
	}

	public static boolean isUseProperties() {
		return useProperties;
	}

	public static void setUseProperties(boolean useProperties) {
		SemanticRelComposition.useProperties = useProperties;
	}

	public static String getTbaDest() {
		return tbaDest;
	}

	public static void setTbaDest(String tbaDest) {
		SemanticRelComposition.tbaDest = tbaDest;
	}

	public static String getPropertiesDest() {
		return propertiesDest;
	}

	public static void setPropertiesDest(String propertiesDest) {
		SemanticRelComposition.propertiesDest = propertiesDest;
	}

	public static int getNumOfAnnotationSamples() {
		return numOfAnnotationSamples;
	}

	public static void setNumOfAnnotationSamples(int numOfAnnotationSamples) {
		SemanticRelComposition.numOfAnnotationSamples = numOfAnnotationSamples;
	}

	public static int[] getWeightThresh() {
		return weightThresh;
	}

	public static void setWeightThresh(int[] weightThresh) {
		SemanticRelComposition.weightThresh = weightThresh;
	}

	public static int getNumOfAnnotationRules() {
		return numOfAnnotationRules;
	}

	public static void setNumOfAnnotationRules(int numOfAnnotationRules) {
		SemanticRelComposition.numOfAnnotationRules = numOfAnnotationRules;
	}

}

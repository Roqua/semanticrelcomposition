package de.uni_mannheim.dws.semanticrelcomposition.pathsearch;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;

import de.uni_mannheim.dws.semanticrelcomposition.rulelearning.Property;
import de.uni_mannheim.dws.semanticrelcomposition.rulelearning.Rule;
import de.uni_mannheim.dws.semanticrelcomposition.utilities.QueryService;
import de.uni_mannheim.dws.semanticrelcomposition.utilities.StorageService;
/**
 * The TFinderSparql implements the Finder interface for RDF knowledge bases queried by SPARQL.
 * Basically this class is responsible for constructing different SPARQL queries and processing the results.
 * @author Kristian Kolthoff
 */
public class FinderSparql implements Finder{

	public static final String PROPERTY = "?p";
	public static final String TOTAL = "?total";
	
	private StorageService sService;
	private QueryService qService;
	private PathFinder tpFinder;
	/**
	 * The current query
	 */
	private Query q;
	/**
	 * The current ResultSet of the current query
	 */
	private ResultSet rs;
	
	public static Logger log = Logger.getLogger(FinderSparql.class.getCanonicalName()); 
	
	public FinderSparql(PathFinder tpFinder) {
		this.sService = new StorageService();
		this.qService = new QueryService(SemanticRelComposition.getSparqlService());
		this.tpFinder = tpFinder;
	}

	/**
	 * Builds up the following SPARQL query with different combinations of 
	 * properties and sets it as the current query:
	 * SELECT ?p (COUNT(?p) AS ?total) 
	 * WHERE {
	 * ?e0 p1 ?e1 . 
	 * ?e1 p2  ?e2 .
	 * ...
	 * ?en-1 pn ?en
	 * ?e0 ?p ?en # for direct heads # ?en ?pd ?e0 # for inverse heads
	 * FILTER (! regex(str(?p), "wiki", "i")) 
	 * FILTER (! isLiteral(?e0)
	 * ...
	 * FILTER (! isLiteral(?en))
	 * FILTER (! regex(?e0, str(?e1))) 
	 * ...
	 * FILTER (! regex(?en-1, str(?en))) 
	 * }
	 * GROUP BY ?p 
	 */
	@Override
	public void onQueryUpdate(boolean isInverse) {
		String query = "SELECT "+ PROPERTY +" (COUNT(" + PROPERTY + ") AS " + TOTAL + ") \n";
		query += "WHERE {\n";
		for (int i = 0; i < tpFinder.getCurrQueryProps().length; i++) {
			query += "?e"+i + " <" + tpFinder.getCurrQueryProps()[i].getName() + "> " +" ?e"+ (i+1) + " . \n";
		}
		if(isInverse) {
			query += "?e" + SemanticRelComposition.getHops() + " " + PROPERTY +" ?e0 \n";
		} else {
			query += "?e0 " + PROPERTY + " ?e" + SemanticRelComposition.getHops() + "\n";
		}
		query += "FILTER (! regex(str(" + PROPERTY + "), 'wiki', 'i')) \n".replace('\'', '"');
		query += "FILTER (! isLiteral(?e0)) \n";
		query += "FILTER (! isLiteral(?e" + SemanticRelComposition.getHops() + "))\n";
		for (int i = 0; i < SemanticRelComposition.getHops(); i++) {
			for (int j = i+1; j <= SemanticRelComposition.getHops(); j++) {
			query += "FILTER (! regex(?e" + i + ", str(?e"+ j+ "))) \n";
			}
		}
		query += "}\n";
		query += "GROUP BY " + PROPERTY + " \n";
		q = QueryFactory.create(query);
	}
	
	/**
	 * Builds up the following SPARQL query with different combinations of 
	 * properties and sets it as the current query:
	 * SELECT ?p1 ?p2 ... ?pn ?pd (COUNT(?pd) AS ?total) 
	 * WHERE {
	 * ?e0 ?p1 ?e1 . 
	 * ?e1 ?p2  ?e2 .
	 * ...
	 * ?en-1 ?pn ?en
	 * ?e0 ?pd ?en # for direct heads # ?en ?pd ?e0 # for inverse heads
	 * FILTER (! regex(str(?p1), "wiki", "i"))
	 * FILTER (regex(?p1, "http://dbpedia.org/ontology/")) 
	 * ...
	 * FILTER (! regex(str(?pn), "wiki", "i"))
	 * FILTER (regex(?pn, "http://dbpedia.org/ontology/")) 
	 * FILTER (! regex(str(?pd), "wiki", "i"))
	 * FILTER (regex(?pd, "http://dbpedia.org/ontology/")) 
	 * FILTER (! isLiteral(?e0)
	 * ...
	 * FILTER (! isLiteral(?en))
	 * FILTER (! regex(?e0, str(?e1))) 
	 * ...
	 * FILTER (! regex(?en-1, str(?en))) 
	 * }
	 * GROUP BY ?p1 ?p2 ... ?pn ?pd 
	 */
	@Override
	public void directQuery(boolean isInverse) {
		String query =  "PREFIX owl: <http://www.w3.org/2002/07/owl#>\n"
				+ "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n"
				+ "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n";
		query += "SELECT ";
		for (int i = 0; i <SemanticRelComposition.getHops(); i++) {
			query += PROPERTY + (i+1) + " ";
		}
		query += " ?pd (COUNT(?pd) AS " + TOTAL + ") FROM <http://dbpedia.org> \n";
		query += "WHERE {\n";
		for (int i = 0; i < SemanticRelComposition.getHops(); i++) {
			query += PROPERTY + (i+1) + " rdf:type owl:ObjectProperty .\n";
		}
		query += "?pd rdf:type owl:ObjectProperty . \n";
		for (int i = 0; i < SemanticRelComposition.getHops(); i++) {
			query += "?e"+i + " " + PROPERTY + (i+1) +" ?e"+ (i+1) + " . \n";
		}
		if(isInverse) {
			query += "?e" + SemanticRelComposition.getHops() + " ?pd ?e0 \n";
		} else {
			query += "?e0 ?pd ?e" + SemanticRelComposition.getHops() + "\n";
		}
		for (int i = 0; i < SemanticRelComposition.getHops(); i++) {
			query += "FILTER (! regex(str(" + PROPERTY +(i+1)+"), 'wiki', 'i')) \n".replace('\'', '"');
			query += "FILTER (regex(" + PROPERTY + (i+1) + ", 'http://dbpedia.org/ontology/')) \n".replace('\'', '"');
		}
		query += "FILTER (! regex(str(?pd), 'wiki', 'i')) \n".replace('\'', '"');
		query += "FILTER (! isLiteral(?e0)) \n";
		query += "FILTER (! isLiteral(?e" + SemanticRelComposition.getHops() + "))\n";
		for (int i = 0; i < SemanticRelComposition.getHops(); i++) {
			for (int j = i+1; j <= SemanticRelComposition.getHops(); j++) {
			query += "FILTER (! regex(?e" + i + ", str(?e"+ j+ "))) \n";
			}
		}
		query += "}\n";
		query += "GROUP BY ";
		for (int i = 0; i < SemanticRelComposition.getHops(); i++) {
			query += "?p" + (i+1) + " ";
		}
		query += "?pd \n";
		q = QueryFactory.create(query);
	}
	
	@Override
	public void storeDir(boolean isInverse) {
		int count = 0;
		while(rs.hasNext()) {
			QuerySolution qs = rs.next();
			String[] resProps = new String[SemanticRelComposition.getHops()+1];
			for (int i = 0; i < SemanticRelComposition.getHops(); i++) {
				resProps[i] = qs.getResource(PROPERTY + (i+1)).getURI();
			}
			resProps[SemanticRelComposition.getHops()] = qs.getResource("?pd").getURI();
			for (int i = 0; i < resProps.length; i++) {
				sService.buffer(resProps[i] + "\t");			
			}
			sService.buffer(String.valueOf(isInverse) + "\t" + qs.getLiteral("total").getInt());
			sService.write();
			count++;
		}
		log.info("[Write:] " + count);
	}
	

	@Override
	public void excecQuery() {
		rs = qService.querySelect(q);
	}

	@Override
	public boolean hasResults() {
		return rs.hasNext();
	}
	
	@Override
	public void storeResults(boolean isInverse) {
		int rsCount = 0;
		log();
		while(rs.hasNext()) {
			QuerySolution qs = rs.next();
			Rule tr = null;
			ArrayList<Property> propsBody = new ArrayList<>();
			for (int i = 0; i < tpFinder.getCurrQueryProps().length; i++) {
				propsBody.add(tpFinder.getCurrQueryProps()[i]);
			}
			try{
				tr = new Rule(propsBody, new Property(qs.getResource(PROPERTY).getURI()), isInverse);
				tr.setCount(qs.getLiteral(TOTAL).getInt());
			} catch(ClassCastException ec) {
				/**This can happen if trying to cast a result literal to a resource. As literals are not relevant we can ignore this**/ 
			}
			tpFinder.verifyChunk();
			rsCount++;
			sService.storeTRule(tr);
		}
		log.info("[ID:] "+ tpFinder.getThreadID() +" ----------------->>> rs-size: " + rsCount);
	}
	
	
	/**		
	 * Logs the current query properies of the body of the rules.
	 */
	protected void log() {
		StringBuilder sb = new StringBuilder("[Store:] " + "ID: " + tpFinder.getThreadID());
		for (int i = 0; i < tpFinder.getCurrQueryProps().length; i++) {
			sb.append(tpFinder.getCurrQueryProps()[i].getName() + " ");
		}
		log.info(sb.toString());
	}
	
	@Override
	public boolean isCompatible() {
		for (int i = 0; i < tpFinder.getCurrQueryProps().length-1; i++) {
			Property p1 = tpFinder.getCurrQueryProps()[i];
			Property p2 = tpFinder.getCurrQueryProps()[i+1];
			String[] p2subDomain = p2.getSubClassHierachyDomain();
			/**
			 * Only if this condition is true, both properties have subclass hierachies for the domain
			 * and the range, respectively. Otherwise this pruning can not be applied.
			 */
			if(p1.getSubClassHierachyDomain() != null && p2subDomain != null) {
				for (int j = 0; j < p2subDomain.length; j++) {
					if(p1.getRange().equals(p2subDomain[j])) {
						return true;
					}
				}
				return false;
			}
		}
		return true;
	}


	@Override
	public void closeConnection() {
		/**
		 * Not neccessary in SPARQL
		 */
	}

	@Override
	public void createFile(String path) {
		sService.createFile(path);
	}


	public QueryService getqService() {
		return qService;
	}


	public void setqService(QueryService qService) {
		this.qService = qService;
	}
}
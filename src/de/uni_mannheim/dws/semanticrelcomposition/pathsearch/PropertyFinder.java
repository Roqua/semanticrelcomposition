package de.uni_mannheim.dws.semanticrelcomposition.pathsearch;

import java.io.File;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Resource;

import de.uni_mannheim.dws.semanticrelcomposition.rulelearning.Property;
import de.uni_mannheim.dws.semanticrelcomposition.utilities.QueryService;
import de.uni_mannheim.dws.semanticrelcomposition.utilities.StorageService;
/**
 * The PropertyFinder extracts property lists from DBPedia and YAGO
 * @author Kristian Kolthoff
 */
public class PropertyFinder{
	
	private StorageService sService;
	private QueryService qService;
	private ArrayList<Resource> propertyList;
	public static Logger log = Logger.getLogger(PropertyFinder.class.getCanonicalName());

	public PropertyFinder(String sparqlService) {
		this.sService = new StorageService();
		this.qService = new QueryService(sparqlService);
		this.propertyList = new ArrayList<>();
	}
	
	public static void main(String[] args) {
		PropertyFinder p = new PropertyFinder(SemanticRelComposition.SPARQL_SERVICE_PUBLIC);
		p.findDBPropertiesProp();
	}
	
	/**
	 * Finds all dbpedia-owl properties which are ObjectProperties and not empty
	 */
	public void findDBPropertiesProp() {
		String query = 
				"PREFIX dbo: <http://dbpedia.org/ontology/> \n"
				+ "PREFIX owl: <http://www.w3.org/2002/07/owl#>\n"
				+ "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n"
				+ "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n"
				+ "SELECT DISTINCT ?property\n "
				+ "WHERE{\n"
				+ "?property rdf:type owl:ObjectProperty .\n"
				+ "?property rdfs:isDefinedBy <http://dbpedia.org/ontology/> .\n"
				+ "}\n"
				+ "ORDER BY ?property\n";
		
		ResultSet qResults = qService.querySelect(query);
		
		while(qResults.hasNext()) {
			QuerySolution qSol = qResults.next();
			propertyList.add(qSol.getResource("property"));
		}
		
		sService.storeProperties(propertyList, new File("").getAbsolutePath() + "/results/DBPedia/Properties/dbpedia-owl-filter1.prop");
		ArrayList<String> properties = sService.readPropertiesAsString(new File("").getAbsolutePath() + "/results/DBPedia/Properties/dbpedia-owl-filter1.prop");
		ArrayList<String> filteredProperties = filterEmptyProperties(properties);
		sService.storePropertiesString(filteredProperties, new File("").getAbsolutePath() + "/results/DBPedia/Properties/dbpedia-owl-filter2.prop");
	}
	
	/**
	 * Removes all DBPedia properties (URI given as a string) containing no entries
	 * @param properties
	 * @return the extracted properties
	 */
	private ArrayList<String> filterEmptyProperties(ArrayList<String> properties) {
		int oldSize = properties.size();
		ArrayList<String> fList = new ArrayList<>();
		for(int i = 0; i < properties.size(); i++) {
			String currProperty = properties.get(i);
			String query = 
					"ASK { ?s "
					+ "<" + currProperty + ">"
					+ " ?o }";
			boolean empty = qService.queryAsk(query);
			if(empty) {
				fList.add(currProperty);
			}
			System.out.print(query+"  --  "+ empty);
			
			System.out.println();
		}
		int newSize = fList.size();
		System.out.println("OldSize: " + oldSize + " NewSize: " + newSize + " Ratio: " + (newSize/(double)(oldSize)));
		return fList;
	}
	
	/**
	 * Returns the currently refrenced property list
	 * @param path - the path to the property list
	 * @return the property list
	 */
	public static ArrayList<Property> loadLocalPropList(String path) {
		return new StorageService().readPropertiesAsProperty(path);
	}
}

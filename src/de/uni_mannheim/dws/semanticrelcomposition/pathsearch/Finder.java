package de.uni_mannheim.dws.semanticrelcomposition.pathsearch;
/**
 * Finder interface is implemented for querying various knowledge bases with different representations and different query languages.
 * Currently the Finder supports the two query languages SPARQL and SQL
 * @author Kristian Kolthoff
 */
public interface Finder {

	/**
	 * Current supported knowledge base queries
	 */
	public static final int SPARQL = 0;
	public static final int SQL = 1;
	
	/**
	 * Called for building up a query for discovering n-Hop rules beetween two entities
	 * **/
	public void onQueryUpdate(boolean isInverse);
	
	/**
	 * Verifies weather a specific combination of connecting body properties is excecutable.
	 * Therefore a type check of the range and domain types of the joining properties needs to be done, respectively.
	 * If this returns false, a pruning of the checked combination can be done.
	 */
	public boolean isCompatible();
	/**
	 * Called for formating and finally storing the received results sets.
	 */
	public void storeResults(boolean isInverse);
	/**
	 * 
	 */
	public void excecQuery();
	/**
	 * 
	 * @return true if the query with the current property combination
	 */
	public boolean hasResults();
	
	/**
	 * All set up connections like database connections should be closed here.
	 */
	public void closeConnection();
	/**
	 * Create a new file with the current <code>StorageService</code> object.
	 * @param path
	 */
	public void createFile(String path);
	/**
	 * Builds up the abstract direct query
	 * @param isInverse - true if the query should look up for inverse heads otherwise false
	 */
	public void directQuery(boolean isInverse);
	/**
	 * Stores the results from the direct query
	 * @param isInverse - true if the query results are from an inverse query otherwise false
	 */
	public void storeDir(boolean isInverse);
	
}

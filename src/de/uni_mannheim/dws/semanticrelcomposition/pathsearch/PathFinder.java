package de.uni_mannheim.dws.semanticrelcomposition.pathsearch;

import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;



import de.uni_mannheim.dws.semanticrelcomposition.rulelearning.Property;

/**
 * TPathFinder is responsible for finding n-hop transitive paths from one
 * entity to another entity and extracting the raw first-order logic rules from
 * the knowledge bases.
 * @author Kristian Kolthoff
 */
public class PathFinder implements Runnable{

	/**
	 * The limit of result entries for one result file
	 */
	public static final int CHUNK_SIZE = 200000;
	/**
	 * Counter for storing the current property counter
	 */
	private int[] counter;
	/**
	 * The property list that is used to query the knowledge base
	 */
	private ArrayList<Property> propertyList;
	/**
	 * The current n-hop property combination
	 */
	private Property[] currQueryProps;
	/**
	 * Singalizes if this TPathFinder is finsihed with testing the property combinations
	 */
	private boolean finished;
	/**
	 * Number of results currently loaded into the chunk
	 */
	private int currChunkSize;
	/**
	 * Number of already written chunks
	 */
	private int currChunk;

	private String fileName;
	private String rawDest;
	/**
	 * The number of queries already excecuted for estimation
	 */
	private int count = 0;
	/**
	 * Counter offsets x and y from where this TPathFinder starts the property combinations
	 */
	private int indexX;
	private int indexY;
	private int threadID;
	/**
	 * The finder implementation that finally excecutes the queries on the knowledge base
	 */
	private Finder finder;
	
	public static Logger log = Logger.getLogger(PathFinder.class.getCanonicalName());
	
	public PathFinder(int hops, int indexX, int indexY, int threadID, int finderType) {
		this.counter = new int[hops];
		this.currQueryProps = new Property[hops];
		this.indexX = indexX;
		this.indexY = indexY;
		this.fileName = SemanticRelComposition.getPrefix();
		this.rawDest = SemanticRelComposition.getRawchunkDest();
		this.threadID = threadID;
		this.currChunk = 1;
		this.currChunkSize = 0;
		this.finished = false;
		this.propertyList = SemanticRelComposition.getPropertyList();
		initFinder(finderType);
		this.finder.createFile(rawDest + fileName + "-" + threadID + "-" + currChunk + ".rs");
	}
	
	/**
	 * Initializes the finder type. Currently this TPathFinder supports knowledge bases which are queried with
	 * SPARQL or SQL
	 * @param finderType - the type of the knowledge base queries
	 */
	private void initFinder(int finderType) {
		if(finderType == Finder.SPARQL) {
			finder = new FinderSparql(this);
		} else if(finderType == Finder.SQL) {
			try {
				finder = new FinderSQL(this);
			} catch (SQLException e) {
				e.printStackTrace();
				System.exit(0);
			}
		}
	}

	/**
	 * Runs the FindTrasRelN algorithm if the useproperties flag of the config.xml is set to true.
	 * Otherwise direct abstract queries are excecuted on the knowledge base.
	 */
	@Override
	public void run() {
		if(SemanticRelComposition.isUseProperties()) {
			findSemRelComp();						
		} else {
			if(threadID == 1) {
				directRelFinder(false);
			} else if(threadID == 2) {
				directRelFinder(true);
			}
		}
	}
	
	/**
	 * Excecutes the direct knowledge base queries
	 */
	public void directRelFinder(boolean isInverse) {
		finder.directQuery(isInverse);
		finder.excecQuery();
		if(finder.hasResults()) {
			finder.storeDir(isInverse);			
		}
		SemanticRelComposition.threadsOver[threadID-1] = true;
		/**
		 * Close potentially open database connections
		 */
		finder.closeConnection();
		/**
		 * Check if all TPathFinder threads are done to continue the rule derivation process
		 */
		SemanticRelComposition.onThreadsOver();
	}
	
	/**
	 * Initializes the counter with the index x and the further counter with 0.
	 */
	private void initCounter() {
		for (int i = 0; i < counter.length; i++) {
			if(i == 0) {
				counter[i] = indexX ;
			} else {
				counter[i] = 0;				
			}
		}
	}
		
		/**
		 * Updates the relevant counter from the counter array.
		 */
		public void onCountUpdate() {
			boolean counted = false;
			for (int i = counter.length-1; i >= 0; i--) {
				if(!counted){
					if(counter[i] >= propertyList.size()-1) {
						if(i==1 && counter[0] == indexY) {
							finished = true;
							return;
						}
						counter[i] = 0;
					} else {
						counter[i] += 1;
						counted = true;
						break;
					}
				}
			}
		}
		
		/**
		 * Sets the current property combination to check according to the current counter values.
		 */
		public void onPropUpdate() {
			for (int i = 0; i < counter.length; i++) {
				currQueryProps[i] = propertyList.get(counter[i]);
			}
		}
		
		
		/**
		 * Checks whether the current chunk file is full. If so, a new chunk file will be created.
		 */
		protected void verifyChunk() {
			currChunkSize++;
			if(currChunkSize > CHUNK_SIZE) {
				currChunk += 1;
				finder.createFile(rawDest + fileName + threadID + "-" + currChunk +".rs");
				currChunkSize = 0;
			}
		}
		
		/**
		 * Main routine traversing all n-combinations of the property list
		 */
		public void findSemRelComp() {
			initCounter();
			while(!finished) {
				count++;
				SemanticRelComposition.speedVec[threadID-1] = count;
				/**
				 * Update the propery combination to build the query on
				 */
				onPropUpdate();
				log();
				try {
					/**
					 * Firstly check if the property conjunction is compatible
					 */
					if(finder.isCompatible()) {
						/**
						 * Querying for direct rule heads
						 */
						finder.onQueryUpdate(false);
						finder.excecQuery();
						if(finder.hasResults()) {
							finder.storeResults(false);
						}
						/**
						 * Querying for inverse rule heads
						 */
						finder.onQueryUpdate(true);
						finder.excecQuery();
						if(finder.hasResults()) {
							finder.storeResults(true);
						}
					}
				} catch(Exception e) {
					log.error(threadID + " " + e.toString());
					e.printStackTrace();
					try {
						/**
						 * After an error occurred while querying the knowledge base,
						 * the thread pauses 60 seconds.
						 */
						Thread.sleep(SemanticRelComposition.TIMEOUT);
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}
				}
				/**
				 * Increment the counter to the next property combination
				 */
				onCountUpdate();
			}
			SemanticRelComposition.threadsOver[threadID-1] = true;
			/**
			 * Close potentially open database connections
			 */
			finder.closeConnection();
			/**
			 * Check if all TPathFinder threads are done to continue the rule derivation process
			 */
			SemanticRelComposition.onThreadsOver();
		}
		
		/**		
		 * Logs the current query properies of the body of the rules.
		 */
		protected void log() {
			StringBuilder sb = new StringBuilder("[Test:] " + "ID: " + threadID + " QAv: " + SemanticRelComposition.compQueryAv() + " ");
			for (int i = 0; i < currQueryProps.length; i++) {
				sb.append(currQueryProps[i].getName() + " ");
				sb.append(counter[i] + " ");
			}
			log.info(sb.toString());
		}

		public int getIndexX() {
			return indexX;
		}

		public void setIndexX(int indexX) {
			this.indexX = indexX;
		}

		public int getIndexY() {
			return indexY;
		}

		public void setIndexY(int indexY) {
			this.indexY = indexY;
		}

		public int getThreadID() {
			return threadID;
		}

		public void setThreadID(int threadID) {
			this.threadID = threadID;
		}

		public int[] getCounter() {
			return counter;
		}

		public void setCounter(int[] counter) {
			this.counter = counter;
		}

		public Property[] getCurrQueryProps() {
			return currQueryProps;
		}

		public void setCurrQueryProps(Property[] currQueryProps) {
			this.currQueryProps = currQueryProps;
		}
		
		

}

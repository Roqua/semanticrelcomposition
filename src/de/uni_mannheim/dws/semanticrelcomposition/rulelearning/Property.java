package de.uni_mannheim.dws.semanticrelcomposition.rulelearning;

/**
 * This Property class represents an property of a knowledge base which connects to entities.
 * @author Kristian Kolthoff
 */
public class Property {

	private String name;
	private String domain = "";
	private String range = "";
	/**
	 * The subClassHierachyDomain beginning with the most upperclass and
	 * ending with the lowest subclass for the domain of the property
	 */
	private String[] subClassHierachyDomain;
	/**
	 * The subClassHierchyRange beginning with the most upperclass and
	 * ending with the lowest subclass for the range of the property
	 */
	private String[] subClassHierchyRange;
	
	public Property(String name) {
		this.name = name;
	}
	
	public Property(String name, String domain, String range) {
		this.name = name;
		this.domain = domain;
		this.range = range;
	}
	
	public Property(String name, String[] subClassHierachyDomain, String[] subClassHierachyRange) {
		this.name = name;
		this.domain = subClassHierachyDomain[0];
		this.range = subClassHierachyRange[0];
		this.subClassHierachyDomain = subClassHierachyDomain;
		this.subClassHierchyRange = subClassHierachyRange;
	}
	
	/**
	 * Compares to Properties
	 */
	@Override
	public boolean equals(Object o) {
		if(o == this) {
			return true;
		}
		if(!(o instanceof Property)) {
			return false;
		}
		Property p = (Property) o;
		if(this.name.equals(p.getName())) {
			return true;
		} else {
			return false;
		}
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public String getRange() {
		return range;
	}
	public void setRange(String range) {
		this.range = range;
	}
	
	public String[] getSubClassHierachyDomain() {
		return subClassHierachyDomain;
	}

	public void setSubClassHierachyDomain(String[] subClassHierachyDomain) {
		this.subClassHierachyDomain = subClassHierachyDomain;
	}

	public String[] getSubClassHierchyRange() {
		return subClassHierchyRange;
	}

	public void setSubClassHierchyRange(String[] subClassHierchyRange) {
		this.subClassHierchyRange = subClassHierchyRange;
	}
	
}
package de.uni_mannheim.dws.semanticrelcomposition.rulelearning;

import java.util.ArrayList;
/**
 * The TRule represents an extracted transitive first-order logic rule.
 * @author Kristian Kolthoff
 *
 */
public class Rule implements Comparable<Rule>{

	/**
	 * The properties of the body of the Rule
	 */
	private ArrayList<Property> propsBody;
	/**
	 * The head property
	 */
	private Property propHead;
	/**
	 * The weight of the rule is equals to the count
	 */
	private int count;
	/**
	 * The confidence value of the rule
	 */
	private double conf;
	/**
	 * The support value of the rule
	 */
	private double support;
	/**
	 * This flag specifies whether the head of the rule connects
	 * the head and tail entity directly or inversely
	 */
	private boolean isInverse;
	
	public Rule(ArrayList<Property> propsBody, Property propHead, boolean isInverse) {
		this.propsBody = propsBody;
		this.propHead = propHead;
		this.isInverse = isInverse;
	}

	public ArrayList<Property> getPropsBody() {
		return propsBody;
	}

	public void setPropsBody(ArrayList<Property> propsBody) {
		this.propsBody = propsBody;
	}

	public Property getPropHead() {
		return propHead;
	}

	public void setPropHead(Property propHead) {
		this.propHead = propHead;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	@Override
	public boolean equals(Object o) {
		if(o == this) {
			return true;
		}
		if(!(o instanceof Rule)) {
			return false;
		}
		Rule t = (Rule) o;
		for (int i = 0; i < propsBody.size(); i++) {
			if(!t.getPropsBody().get(i).equals(this.propsBody.get(i))) {
				return false;	
			}
		}
		if(this.propHead.equals(t.getPropHead()) && (this.isInverse == t.isInverse())) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public String toString() {
		String tr = "";
		for (int i = 0; i < propsBody.size(); i++) {
			tr += propsBody.get(i).getName() + "\t";
		}
		tr += propHead.getName() + "\t";
		tr += String.valueOf(count) + "\t";
		tr += String.valueOf(conf) + "\t";
		tr += String.valueOf(support) + "\t";
		if(isInverse) {
			tr += "true";
		} else {
			tr += "false";
		}
		return tr;
	}

	public boolean isInverse() {
		return isInverse;
	}

	public void setInverse(boolean isInverse) {
		this.isInverse = isInverse;
	}

	@Override
	public int compareTo(Rule o) {
		if(conf < o.getConf()) {
			return 1;
		} else if(conf == o.getConf()) {
			return 0;
		} else {
			return -1;
		}
	}

	public double getConf() {
		return conf;
	}

	public void setConf(double conf) {
		this.conf = conf;
	}

	public double getSupport() {
		return support;
	}

	public void setSupport(double support) {
		this.support = support;
	}
}
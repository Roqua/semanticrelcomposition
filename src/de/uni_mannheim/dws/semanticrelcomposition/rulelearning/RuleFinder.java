package de.uni_mannheim.dws.semanticrelcomposition.rulelearning;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import org.apache.log4j.Logger;

import de.uni_mannheim.dws.semanticrelcomposition.pathsearch.Finder;
import de.uni_mannheim.dws.semanticrelcomposition.pathsearch.SemanticRelComposition;
import de.uni_mannheim.dws.semanticrelcomposition.utilities.DBService;
import de.uni_mannheim.dws.semanticrelcomposition.utilities.Encoder;
import de.uni_mannheim.dws.semanticrelcomposition.utilities.QueryService;
import de.uni_mannheim.dws.semanticrelcomposition.utilities.StorageService;
/**
 * Processed the raw extraction results from the TPathFinder and produces the final rules
 * with confidence and support values.
 * @author Kristian Kolthoff
 *
 */
public class RuleFinder implements Runnable{

	private StorageService sService;
	private QueryService qService;
	private DBService dbService;
	private boolean finished;
	/**
	 * All found TRules
	 */
	private ArrayList<Rule> tRulesAll;
	/**
	 * Two rule sets for dbpedia with different head property types
	 */
	private ArrayList<Rule> tRulesdbp;
	private ArrayList<Rule> tRulesdbo;
	/**
	 * The number of tuple instances for each property body
	 */
	private HashMap<String, Integer> hCount;
	private int chunkID;
	private int currChunk;
	private String dest;
	private String file;
	private int count;
	private int[] thresholds;
	
	public static Logger log = Logger.getLogger(RuleFinder.class.getCanonicalName());
	
	public RuleFinder() {
		this.sService = new StorageService();
		this.finished = false;
		this.count = 0;
		this.hCount = new HashMap<>();
		this.tRulesAll = new ArrayList<>();
		this.tRulesdbp = new ArrayList<>();
		this.tRulesdbo = new ArrayList<>();
		this.dest = SemanticRelComposition.getRuleDest();
		this.file = SemanticRelComposition.getPrefix();
		this.thresholds = SemanticRelComposition.getWeightThresh();
		this.qService = new QueryService(SemanticRelComposition.getSparqlService());
		try {
			this.dbService = new DBService(SemanticRelComposition.getDatabase(), SemanticRelComposition.getUser(), SemanticRelComposition.getPassword());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		this.chunkID = 1;
		this.currChunk = 0;
	}

	@Override
	public void run() {
		findTRules();
	}
	
	public static void main(String[] args) {
		SemanticRelComposition tr = new SemanticRelComposition();
		tr.basicConfig();
		RuleFinder trf = new RuleFinder();
		new Thread(trf).start();
	}

	/**
	 * Finds all n-hop transitive rules from a given file.
	 */
	public void findTRules() {
		loadChunk();
		while(!finished) {
			if(sService.hasNextResult()) {
				try{
					Rule tr = sService.readResRule();
					saveRule(tr);
				}catch(Exception e) {	
				}
			} else {
				loadChunk();
			}
		}
		compSupport();
		writeRules(true);
		SemanticRelComposition.onTRuleFinderOver();
	}
	
	/**
	 * Counts the total number of rules, the number of inverse rules,
	 * the number of class A rules and class B rules.
	 */
	public void stats(ArrayList<Rule> tRules, int weightThresh) {
		int total = 0;
		int inv = 0;
		int aRules = 0;
		int bRules = 0;
		StorageService sService = new StorageService();
		sService.createFile(dest + file + "-" + weightThresh + "-stats");
		for (Rule tr : tRules) {
			total++;
			if (tr.isInverse()) {
				inv++;
			}
			boolean eqProps = true;
			for (int i = 0; i < tr.getPropsBody().size(); i++) {
				Property p1 = tr.getPropsBody().get(i);
				if (!p1.equals(tr.getPropHead())) {
					eqProps = false;
					break;
				}
				for (int j = 0; j < tr.getPropsBody().size(); j++) {
					Property p2 = tr.getPropsBody().get(j);
					if (!p1.equals(p2)) {
						eqProps = false;
						break;
					}
				}
			}
			if (eqProps) {
				sService.buffer(tr.getPropHead().getName());
				sService.write();
				bRules++;
			}
			boolean neqProps = true;
			for (int i = 0; i < tr.getPropsBody().size(); i++) {
				Property p1 = tr.getPropsBody().get(i);
				if (p1.equals(tr.getPropHead())) {
					neqProps = false;
					break;
				}
				for (int j = 0; j < tr.getPropsBody().size(); j++) {
					Property p2 = tr.getPropsBody().get(j);
					if ((i != j) && p1.equals(p2)) {
						neqProps = false;
						break;
					}
				}
			}
			if (neqProps) {
				aRules++;
			}

		}
		String stats = "#Total: " +total + " #Inverse: " + inv + " #A-Rules: " + aRules + " #B-Rules: " + bRules;
		log.info(stats);
		sService.buffer(stats);
		sService.write();
	}
	
	
	/**
	 * Saves a single TRule and computes the confidence value
	 * @param tr - the TRule to save
	 */
	public void saveRule(Rule tr) {
		String body = "";
		count++;
		for (int i = 0; i < tr.getPropsBody().size(); i++) {
			body += tr.getPropsBody().get(i).getName() + " ";
		}
		if(hCount.containsKey(body)) {
			log.info("[Conf Found] " + count);
			float cf = tr.getCount() / (float) hCount.get(body);
			tr.setConf(cf);
		} else {
			log.info("[Conf query] " + count);
			int sum =  getBodySum(tr);
			float cf = tr.getCount() / (float) sum;
			tr.setConf(cf);
			hCount.put(body, sum);
		}
		tRulesAll.add(tr);
	}
	
	/**
	 * Returns the number of instances for a specific property conjunction.
	 * This value is required for calculating the confidence value.
	 * @param tr - the TRule with the property conjunction in the body of the rule
	 * @return the number of the (n+1) tuple instances (e0,e1,...,en)
	 */
	public int getBodySum(Rule tr) {
		int ft = SemanticRelComposition.getFinderType();
		if(ft == Finder.SPARQL) {
			return qService.queryBodyCount(tr.getPropsBody());
		} else if(ft == Finder.SQL) {
			return dbService.queryBodyCount(tr.getPropsBody());
		}
		return 0;
	}
	
	/**
	 * Computes the support of the rules
	 */
	public void compSupport() {
		log.info("[Computing supp]");
		float totalSize = 0;
		for(Rule tr : tRulesAll) {
			totalSize += tr.getCount();
		}
		for(Rule tr : tRulesAll) {
			float support = tr.getCount() / totalSize;
			tr.setSupport(support);
		}
	}

	/**
	 * Writes the specific rules of DBPedia to a file and creates to seperated files.
	 * The first one contains only dbpedia-owl: prefixed properties in the head.
	 * The second one contains only dbpprop: prefixed properties in the head.
	 * @param sorted
	 */
	public void writeRulesDBPedia(boolean sorted) {
		log.info("[Saving rules]");
		for (int i = 0; i < tRulesAll.size(); i++) {
			Rule tr = tRulesAll.get(i);
			if(tr.getPropHead().getName().contains("property")) {
				tRulesdbp.add(tr);
			} else if(tr.getPropHead().getName().contains("ontology")) {
				tRulesdbo.add(tr);
			}
		}
		writeTopK(tRulesdbo, true, "dbo");
		writeTopK(tRulesdbp, true, "dbp");
	}

	/**
	 * Write the specific rules
	 * @param sorted
	 */
	public void writeRules(boolean sorted) {
		log.info("[Saving rules]");
		writeTopK(tRulesAll, true, "");
	}
	
	/**
	 * Writes the top-k rules to a file with the specified weight thresholds in the config.xml
	 * @param tRules - the rules that should be written with weight thresholds
	 * @param sorted - if true, the rules are sorted by the confidence values
	 * @param type - an additional prefix for the resulting rule file
	 */
	public void writeTopK(ArrayList<Rule> tRules, boolean sorted, String type) {
		for (int i = 0; i < thresholds.length; i++) {
			log.info("[Saving] weightThreshold: " + thresholds[i]);
			ArrayList<Rule> currRules = new ArrayList<>();
			for (int j = 0; j < tRules.size(); j++) {
				Rule tr = tRules.get(j);
				if(tr.getCount() >= thresholds[i]) {
					currRules.add(tRules.get(j));
				}
			}
			sService.createFile(dest + file + "-" + thresholds[i] + type);
			if(sorted) {
				Collections.sort(currRules);			
			}
			stats(currRules,thresholds[i]);
			prettyRulePrint(currRules, thresholds[i]);
			sService.storeTRuleTSV(currRules);
		}
	}
	
	
	private void prettyRulePrint(ArrayList<Rule> tRules, int weight) {
		StorageService sService = new StorageService();
		Encoder enc = new Encoder();
		sService.createFile(dest + file + "-" + weight  + "-finalRules");
		for(Rule tr : tRules) {
			String rule = tr.getCount() + " : " + enc.decodeTRuleYago(tr) + "\t" + tr.getConf() + "\t" + tr.getSupport();
			sService.buffer(rule);
			sService.write();
		}
	}
	
	/**
	 * Loads the raw extraction results and each single chunk
	 */
	public void loadChunk() {
		currChunk++;
		try {
			String dest = SemanticRelComposition.getRawchunkDest() + file + "-"+ chunkID + "-" + currChunk + ".rs";
			sService.initReader(dest, 0);
			log.info("[Loading chunk] " + chunkID + "-" + currChunk);
		} catch (Exception e1) {
			chunkID++;
			if(chunkID > SemanticRelComposition.getNumOfThreads()) {
				/**
				 * Now all chunks from all threads are read
				 */
				finished = true;
				return;
			} else {
				try {
					/**
					 * There is another thread to read chunks from
					 */
					currChunk = 1;
					sService.initReader(SemanticRelComposition.getRawchunkDest() + file + "-" +  chunkID + "-" +currChunk + ".rs", 0);
					log.info("[Loading chunk] " + chunkID + "-" + currChunk);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}			
		}
	}
	

	public boolean isFinished() {
		return finished;
	}

	public void setFinished(boolean finished) {
		this.finished = finished;
	}
}

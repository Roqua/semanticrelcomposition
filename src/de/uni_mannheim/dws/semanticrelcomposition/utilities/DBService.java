package de.uni_mannheim.dws.semanticrelcomposition.utilities;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.SQLDialect;
import org.jooq.SelectQuery;
import org.jooq.impl.DSL;

import static de.uni_mannheim.dws.semanticrelcomposition.knowledge.Tables.*;
import de.uni_mannheim.dws.semanticrelcomposition.knowledge.tables.Knowledgebase;
import de.uni_mannheim.dws.semanticrelcomposition.pathsearch.SemanticRelComposition;
import de.uni_mannheim.dws.semanticrelcomposition.pathsearch.Path;
import de.uni_mannheim.dws.semanticrelcomposition.rulelearning.Property;
import de.uni_mannheim.dws.semanticrelcomposition.rulelearning.Rule;
/**
 * Responsible for doing some basic SQL database queries
 * @author Kristian Kolthoff
 *
 */
public class DBService {

	private Connection con;
	private DSLContext ctx;
	private SelectQuery<Record> query;
	private Result<Record> rs;
	private ArrayList<Knowledgebase> kbTables;
	private ArrayList<List<String>> rsList;
	
	public static Logger log = Logger.getLogger(DBService.class.getCanonicalName());
	
	public DBService(String database, String user, String password) throws SQLException {
		this.con = DriverManager.getConnection(database, user, password);
		this.ctx = DSL.using(con, SQLDialect.POSTGRES);
		this.kbTables = new ArrayList<>();
		this.rsList = new ArrayList<>();
	}
	
	/**
	 * Queries the knowledge base in SQL for rule instances
	 * @param tr - the TRule of which we want to have the instances
	 * @param numOfSamples - the number of instances
	 */
	public void sampleQueryDB(Rule tr, int numOfSamples) {
		query = ctx.selectQuery();
		for (int i = 0; i < SemanticRelComposition.getHops(); i++) {
			Knowledgebase kb = KNOWLEDGEBASE.as("kb" + i);
			if(i == SemanticRelComposition.getHops()-1) {
				query.addSelect(kb.S, kb.P, kb.O);
			} else {
				query.addSelect(kb.S, kb.P);
			}
			kbTables.add(kb);
			query.addFrom(kb);
		}
		Knowledgebase kbD = KNOWLEDGEBASE.as("kbD");
		query.addSelect(kbD.S, kbD.P, kbD.O);
		query.addFrom(kbD);
		kbTables.add(kbD);
		for (int i = 0; i < SemanticRelComposition.getHops()-1; i++) {
			Knowledgebase kbLeft = kbTables.get(i);
			Knowledgebase kbRight = kbTables.get(i + 1);
			query.addConditions(kbLeft.O.equal(kbRight.S));
			query.addConditions(kbLeft.P.eq(tr.getPropsBody().get(i).getName()));
		}
		query.addConditions(kbTables.get(SemanticRelComposition.getHops()-1).P.eq(tr.getPropsBody().get(SemanticRelComposition.getHops()-1).getName()));
		query.addConditions(kbD.P.eq(tr.getPropHead().getName()));
		if(tr.isInverse()) {
			query.addConditions(kbTables.get(0).S.equal(kbD.O));
			query.addConditions(kbTables.get(SemanticRelComposition.getHops()-1).O.equal(kbD.S));			
		} else {
			query.addConditions(kbTables.get(0).S.equal(kbD.S));
			query.addConditions(kbTables.get(SemanticRelComposition.getHops()-1).O.equal(kbD.O));	
		}
		query.addLimit(numOfSamples);
		rs = query.fetch();
		
		rs = query.fetch();
		rsList.clear();
		if(rs.isNotEmpty()) {
		for (int j = 0; j < SemanticRelComposition.getHops(); j++) {
			rsList.add(rs.getValues(kbTables.get(j).S));
			rsList.add(rs.getValues(kbTables.get(j).P));					
		}
		rsList.add(rs.getValues(kbTables.get(SemanticRelComposition.getHops()-1).O));
		rsList.add(rs.getValues(kbTables.get(SemanticRelComposition.getHops()).S));
		rsList.add(rs.getValues(kbTables.get(SemanticRelComposition.getHops()).P));
		rsList.add(rs.getValues(kbTables.get(SemanticRelComposition.getHops()).O));
		}
	}
	
	/**
	 * Extract the number of tuple instances (e1,e2,...,en) that are reachable over the property path
	 * @param props - the property path the instances should be on
	 * @return the number of tuple instances
	 */
	public int queryBodyCount(ArrayList<Property> props) {
		query = ctx.selectQuery();
		kbTables.clear();
		for (int i = 0; i < SemanticRelComposition.getHops(); i++) {
			Knowledgebase kb = KNOWLEDGEBASE.as("kb" + i);
			kbTables.add(kb);
			query.addFrom(kb);
		}
		for (int i = 0; i < props.size()-1; i++) {
			Knowledgebase kbLeft = kbTables.get(i);
			Knowledgebase kbRight = kbTables.get(i + 1);
			query.addConditions(kbLeft.O.equal(kbRight.S));
			query.addConditions(kbLeft.P.eq(props.get(i).getName()));
		}
		query.addConditions(kbTables.get(SemanticRelComposition.getHops()-1).P.eq(props.get(SemanticRelComposition.getHops()-1).getName()));
		query.toString();
		return ctx.fetchCount(query);
	}
	
	/**
	 * 
	 * @return the previously excecuted rule instance results
	 */
	public ArrayList<Path> getResults() {
		ArrayList<Path> tpList = new ArrayList<>();
		for (int i = 0; i < rsList.get(0).size(); i++) {
			Path tp = new Path();
			for (int j = 0; j < rsList.size(); j++) {
				tp.addStep(rsList.get(j).get(i));
				}
			tpList.add(tp);
			}
		return tpList;
		}


	/**
	 * @return Current database connection
	 */
	public Connection getCon() {
		return con;
	}

	public void setCon(Connection con) {
		this.con = con;
	}
}
package de.uni_mannheim.dws.semanticrelcomposition.utilities;
/**
 * This ResultOffsetException is thrown when the reading offset of a result file is bigger than the number of results in a file.
 * @author Kristian Kolthoff
 */
public class ResultOffsetException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4607059137770846571L;

	public ResultOffsetException(int pos, int offset) {
		super("ResultOffsetException with offset :" + offset + " at position: " +pos);
	}
}

package de.uni_mannheim.dws.semanticrelcomposition.utilities;

import java.util.ArrayList;
import java.util.HashMap;

import de.uni_mannheim.dws.semanticrelcomposition.rulelearning.Property;
import de.uni_mannheim.dws.semanticrelcomposition.rulelearning.Rule;
/**
 * The Encoder is responsible for encoding and decoding of DBPedia and YAGO properties
 * and creating proper string representations of the TRules
 * @author Kristian Kolthoff
 *
 */
public class Encoder {
	
	private HashMap<String, String> dbpediaUriMap;
	public static final String RIGHT_ARROW = "  --->>  ";
	public static final String AND = " and ";
	
	public Encoder() {
		this.dbpediaUriMap = new HashMap<>();
		initMap();
	}
	
	/**
	 * Inits the basic prefix mappings for DBPedia
	 */
	public void initMap() {
		dbpediaUriMap.put("ontology", "dbpowl:");
		dbpediaUriMap.put("property", "dbpprop:");
		dbpediaUriMap.put("resource", "dbr:");
	}
	
	/**
	 * Creates a proper string representation of a TRule for YAGO rules
	 * @param tr - the TRule which should be transformed to a proper string representation
	 * @return the string representation of the TRule
	 */
	public String decodeTRuleYago(Rule tr) {
		String rule = "";
		for (int i = 0; i < tr.getPropsBody().size(); i++) {
			rule += removeYagoSyn(tr.getPropsBody().get(i).getName()) + getEnumeration(i);
			if(i != tr.getPropsBody().size()-1) {
				rule += AND;				
			}
		}
		rule += RIGHT_ARROW;
		rule += removeYagoSyn(tr.getPropHead().getName());
		rule += getHeadEnumeration(tr.getPropsBody().size(), tr.isInverse());
		return rule;
	}
	
	
	/**
	 * Trim YAGO properties to get proper predicates
	 * @param s
	 * @return
	 */
	public String removeYagoSyn(String s) {
		return s.replace("<", "").replace(">", "").trim();
	}
	
	/**
	 * Creates a proper string representation of a TRule for DBPedia rules
	 * @param tr - the TRule which should be transformed to a proper string representation
	 * @param prefixOn - if prefixOn is true then the DBPedia properies are prefixed with the 
	 * corresponding prefixes
	 * @return the string representation of the TRule
	 */
	public String decodeTRuleDBPedia(Rule tr, boolean prefixOn) {
		String rule = "";
		for (int i = 0; i < tr.getPropsBody().size(); i++) {
			if(prefixOn) {
				rule += removeDBPediaSyn(tr.getPropsBody().get(i).getName()) + getEnumeration(i);				
			} else {
				rule += removeDBPediaSynCompl(tr.getPropsBody().get(i).getName()) + getEnumeration(i);
			}
			if(i != tr.getPropsBody().size()-1) {
				rule += AND;				
			}
		}
		rule += RIGHT_ARROW;
		if(prefixOn) {
			rule += removeDBPediaSyn(tr.getPropHead().getName());			
		} else {
			rule += removeDBPediaSynCompl(tr.getPropHead().getName());
		}
		rule += getHeadEnumeration(tr.getPropsBody().size(), tr.isInverse());
		return rule;
	}
	
	/**
	 * Trims the DBPedia URIs to proper prefixed prediactes like dbpprop:action
	 * @param s - the URI of the DBPedia property
	 * @return the prefixed property
	 */
	public String removeDBPediaSyn(String s) {
		String[] results = s.split("/");
		String entity = results[results.length-1];
		String prefix = dbpediaUriMap.get(results[results.length-2]);
		return prefix + entity;
	}
	
	/**
	 * Removes the complete URI of the DBPedia property and only returns
	 * the property names
	 * @param s - the URI of the DBPedia property
	 * @return the property name
	 */
	public String removeDBPediaSynCompl(String s) {
		return s.split("/")[s.split("/").length-1];
	}
	
	/**
	 * Creates the string variable tuples for the predicates
	 * @param i - the i-th-Hop predicate
	 * @return the string representation of the variable tuples
	 */
	public String getEnumeration(int i) {
		 return "("+(char)(i+97) + ", "+(char)(i+98)+")";
	}
	
	/**
	 * Creates the string variable tuples for the predicates
	 * @param i - the i-th-Hop predicate
	 * @return the string representation of the variable tuples
	 */
	public String getEnumerationE(int i) {
		 return "(e"+i + ",e"+(i+1)+")";
	}
	
	/**
	 * Creates the string variable tuples for the head predicate of the rule
	 * @param length - the number of the hops of the rules
	 * @param isInverse - true if the head and tail entity are connected inversly
	 * @return the string representation of the head predicate of the rule
	 */
	public String getHeadEnumeration(int length, boolean isInverse) {
		if(isInverse) {
			return "("+(char)(97 + length) + ", "+(char)(97)+")";
		} else {			
			return "("+(char)(97) + ", "+(char)(97 + length)+")";
		}
	}
	
	/**
	 * Creates the string variable tuples for the head predicate of the rule
	 * @param length - the number of the hops of the rules
	 * @param isInverse - true if the head and tail entity are connected inversly
	 * @return the string representation of the head predicate of the rule
	 */
	public String getHeadEnumerationE(int length, boolean isInverse) {
		if(isInverse) {
			 return "(e"+length + ",e0)";
		} else {			
			return "(e0,e"+length+")";
		}
	}
	
	public static void main(String[] args) {
		ArrayList<Property> propsBody = new ArrayList<>();
		propsBody.add(new Property("http://dbpedia.org/ontology/associate"));
		propsBody.add(new Property("http://dbpedia.org/ontology/birthPlace"));
		Rule trYago = new Rule(propsBody, new Property("http://dbpedia.org/ontology/Mark_Wahlberg"), false);
		Encoder encoder = new Encoder();
		System.out.println(encoder.decodeTRuleDBPedia(trYago, true));
	}
	
	
}

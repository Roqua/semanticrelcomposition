package de.uni_mannheim.dws.semanticrelcomposition.utilities;

import java.util.ArrayList;

import com.hp.hpl.jena.graph.NodeFactory;
import com.hp.hpl.jena.graph.Triple;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.sparql.core.Var;
import com.hp.hpl.jena.sparql.expr.E_IsLiteral;
import com.hp.hpl.jena.sparql.expr.E_LogicalNot;
import com.hp.hpl.jena.sparql.expr.E_Regex;
import com.hp.hpl.jena.sparql.expr.E_Str;
import com.hp.hpl.jena.sparql.expr.Expr;
import com.hp.hpl.jena.sparql.expr.ExprVar;
import com.hp.hpl.jena.sparql.syntax.ElementFilter;
import com.hp.hpl.jena.sparql.syntax.ElementGroup;
import com.hp.hpl.jena.sparql.syntax.ElementTriplesBlock;

import de.uni_mannheim.dws.semanticrelcomposition.pathsearch.SemanticRelComposition;
import de.uni_mannheim.dws.semanticrelcomposition.rulelearning.Property;
import de.uni_mannheim.dws.semanticrelcomposition.rulelearning.Rule;

/**
 * Responsible for excecuting SPARQL queries on various endpoints and returning 
 * the corresponding results.
 * @author Kristian Kolthoff
 */
public class QueryService {

	/**
	 * Common PREFIX namespaces for SPARQL queries
	 */
	public static final String DBP_OWL = "PREFIX dbo: <http://dbpedia.org/ontology/>";
	public static final String RDF = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>";
	public static final String RDFS = "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>";
	
	/**
	 * URI of currently used SPARQL endpoint service
	 */
	private String sparqlService;
		
	public QueryService(String sparqlService) {
		this.sparqlService = sparqlService;
	}	
	
	/** 
	 * Queries the current SPARQL endpoint with a SELECT type query
	 * @param query - SPARQL query as a string
	 * @return results of the query
	 */
	public ResultSet querySelect(String query) {
		Query q = QueryFactory.create(query);
		QueryExecution qExcec = QueryExecutionFactory.sparqlService(sparqlService, q);
		return qExcec.execSelect();
	}
	
	/** 
	 * Queries the current SPARQL endpoint with a SELECT type query
	 * @param query - SPARQL query as a Query object
	 * @return results of the query
	 */
	public ResultSet querySelect(Query query) {
		QueryExecution qExcec = QueryExecutionFactory.sparqlService(sparqlService, query);
		ResultSet r = qExcec.execSelect();
		return r;
	}
	
	/**
	 * Extracts sample instances for a transitive first-order logic rule
	 * @param tr - the TRule we want the samples from
	 * @param numOfSamples - the number of samples that should be derived
	 * @return the ResultSet of the query
	 */
	public ResultSet sampleQuery(Rule tr, int numOfSamples) {
		ElementTriplesBlock hopBlock = new ElementTriplesBlock();
		for (int k = 0; k < SemanticRelComposition.getHops(); k++) {
			Triple t = Triple.create(Var.alloc("e" + k), NodeFactory.createURI(tr.getPropsBody().get(k).getName()), Var.alloc("e" +(k+1)));
			hopBlock.addTriple(t);
		}
		/**Find pd - the direct property between entity e0 and entity en*/
		Triple t;
		if(tr.isInverse()) {
			t = Triple.create(Var.alloc("e0"), NodeFactory.createURI(tr.getPropHead().getName()), Var.alloc("e" + SemanticRelComposition.getHops()));	
		} else {
			t = Triple.create(Var.alloc("e0"), NodeFactory.createURI(tr.getPropHead().getName()), Var.alloc("e" + SemanticRelComposition.getHops()));			
		}
		hopBlock.addTriple(t);
		/**Filter1 for removing wikipedia related results in direct properties*/
		Expr eWiki1 = new E_Regex(new E_Str(new ExprVar("h")),"wiki","i");
		Expr notE1 = new E_LogicalNot(eWiki1);
		ElementFilter filter1 = new ElementFilter(notE1);
		/**Filter5 for removing literals*/
		Expr eLiteral = new E_IsLiteral(new ExprVar("e0"));
		Expr noteLiteral = new E_LogicalNot(eLiteral);
		ElementFilter filter3 = new ElementFilter(noteLiteral);
		/**Filter6 for removing literals*/
		Expr eLiteral2 = new E_IsLiteral(new ExprVar("e"+ SemanticRelComposition.getHops()));
		Expr noteLiteral2 = new E_LogicalNot(eLiteral2);
		ElementFilter filter4 = new ElementFilter(noteLiteral2);
		/**Order the results by entity e0*/
		ElementGroup body = new ElementGroup();
		body.addElement(hopBlock);
		body.addElement(filter1);
		body.addElement(filter3);
		body.addElement(filter4);
	
		Query q = QueryFactory.create();
		q.setQueryPattern(body);
		q.setQuerySelectType();
		for (int i = 0; i < SemanticRelComposition.getHops()+1; i++) {
			q.addResultVar("e"+i);
		}
		q.addResultVar("h");
		q.setLimit(numOfSamples);
		return querySelect(q);
	}
	
	/**
	 * Queries the current SPARQL endpoint with an ASK type query
	 * @param query - SPARQL query
	 * @return results of the query
	 */
	public boolean queryAsk(String query) {
		Query q = QueryFactory.create(query);
		QueryExecution qExcec = QueryExecutionFactory.sparqlService(sparqlService, q);
		return qExcec.execAsk();
	}
	
	/**
	 * Queries the current SPARQL endpoint with an ASK type query
	 * @param query - SPARQL query
	 * @return results of the query
	 */
	public boolean queryAsk(Query query) {
		QueryExecution qExcec = QueryExecutionFactory.sparqlService(sparqlService, query);
		return qExcec.execAsk();
	}
	
	/**
	 * Retrives the number of instances for a specific property conjunction
	 * @param props - the property conjunction
	 * @return the number of tuples instances (e1,e2,...,en) that are 
	 * reachable over the property conjunction
	 */
	public int queryBodyCount(ArrayList<Property> props) {
		String q = "SELECT (COUNT(*) as ?total) \n WHERE{";
		for (int i = 0; i < SemanticRelComposition.getHops(); i++) {
			q += "?e" + i + " <"+ props.get(i).getName() + "> ?e"+ (i+1) + " .\n";
		}
		q += "}\n";
		ResultSet rs = querySelect(q);
		if(rs.hasNext()) {
			QuerySolution qs = rs.next();
			return qs.getLiteral("total").getInt();
		}
		return 0;
	}

	public String getSparqlService() {
		return sparqlService;
	}

	public void setSparqlService(String sparqlService) {
		this.sparqlService = sparqlService;
	}

}

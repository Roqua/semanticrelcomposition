package de.uni_mannheim.dws.semanticrelcomposition.utilities;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import com.hp.hpl.jena.rdf.model.Resource;

import de.uni_mannheim.dws.semanticrelcomposition.pathsearch.Path;
import de.uni_mannheim.dws.semanticrelcomposition.rulelearning.Property;
import de.uni_mannheim.dws.semanticrelcomposition.rulelearning.Rule;
/**
 * Responsible for handling all file reading and file writing related processes
 * @author Kristian Kolthoff
 */
public class StorageService {

	public static final String TAB = "\t";
	public static final String SEPERATOR = "--";
	private BufferedWriter bWriter;
	private BufferedReader bReader;
	private String currReadResults;
	
	public StorageService(){
	}

	/**
	 * Creates a new file and sets up a new BufferedWriter to write into this file
	 * @param path - the path where the file is created
	 */
	public void createFile(String path) {
		try {
			File file = new File(path);
			file.createNewFile();
			this.bWriter = new BufferedWriter(new FileWriter(path));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Stores properties extracted from DBPedia as resources
	 * @param resources - the resources representing the properties
	 * @param path - the path where the property file is created
	 */
	public void storeProperties(ArrayList<Resource> resources, String path) {
		File file = new File(path);
			try {
				file.createNewFile();
				this.bWriter = new BufferedWriter(new FileWriter(file));
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			for(Resource r : resources) {
				try {
					bWriter.append(r.getURI());
					bWriter.newLine();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			try {
				bWriter.flush();
				bWriter.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		
	}
	
	/**
	 * Stores properties extracted from YAGO
	 * @param uris - the uris representing the properties
	 * @param path - the path where the property file is created
	 */
	public void storePropertiesString(ArrayList<String> uris, String path) {
		File file = new File(path);
			try {
				this.bWriter = new BufferedWriter(new FileWriter(file));
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			for(String r : uris) {
				try {
					bWriter.append(r);
					bWriter.newLine();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			try {
				bWriter.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
	}

	//TODO remove?
	public void storeResultTSV(Path tp) {
		try {
			bWriter.append(tp.toString());
			bWriter.newLine();
			bWriter.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Writes a list of TRules to the current open file and creates an header
	 * @param tRules - the TRules that should be stored in the file
	 */
	public void storeTRuleTSV(ArrayList<Rule> tRules) {
		/**Create Header**/
		try {
		for (int i = 0; i < tRules.get(0).getPropsBody().size(); i++) {
			bWriter.append("P"+(i+1) + "\t");
		}
		bWriter.append("H\t");
		bWriter.append("W\t");
		bWriter.append("CF\t");
		bWriter.append("SUPP\t");
		bWriter.append("INV\t");
		bWriter.newLine();
			for(Rule tr : tRules) {
			bWriter.append(tr.toString());
			bWriter.newLine();
			bWriter.flush();
		} 
			} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Buffer plain strings on the BufferedWriter. To flush the contents call write()
	 * @param content - content to write into the buffer
	 */
	public void buffer(String content) {
		try {
			bWriter.append(content);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Flushes to buffered content into the file
	 */
	public void write() {
		try {
			bWriter.newLine();
			bWriter.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Writes a single TRule into a file
	 * @param tr
	 */
	public void storeTRule(Rule tr) {
		try {
		for(Property p : tr.getPropsBody()) {
				bWriter.append(p.getName() + "\t");
		}
		bWriter.append(tr.getPropHead().getName() + "\t" + tr.getCount() + "\t" + tr.isInverse());
		bWriter.newLine();
		bWriter.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Reads a TRule from a result file and creates a corresponding TRule object
	 * @return the initialized TRule
	 */
	public Rule readResRule() {
		ArrayList<Property> propsBody = new ArrayList<>();
		String[] results = currReadResults.split(TAB);
		int l = results.length;
		for (int i = 0; i < l-3; i++) {
			propsBody.add(new Property(results[i]));
		}
		Rule tr = null;
		Property propHead = new Property(results[l-3]);
		if(results[l-1].equals("t") || results[l-1].equals("true")) {
			tr = new Rule(propsBody, propHead, true);
		} else if(results[l-1].equals("f") || results[l-1].equals("false")){
			tr = new Rule(propsBody, propHead, false);
		}
		tr.setCount(Integer.valueOf(results[l-2]));
		return tr;
	}

	
	/**
	 * Reads all properties from a local property file and returns the properties as strings
	 * @param path - the path to the property file
	 * @return the property list
	 */
	public ArrayList<String> readPropertiesAsString(String path) {
		ArrayList<String> propertyList = new ArrayList<>();
		try {
			bReader = new BufferedReader(new FileReader(path));
			String line = bReader.readLine();
			while(line != null && !line.isEmpty()) {
				propertyList.add(line);
				line = bReader.readLine();
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		return propertyList;
	}
	
	/**
	 * Reads all properties from a local property file and returns the properties as Property objects
	 * @param path - the path to the property file
	 * @return the property list
	 */
	public ArrayList<Property> readPropertiesAsProperty(String path) {
		ArrayList<Property> propertyList = new ArrayList<>();
		try {
			bReader = new BufferedReader(new FileReader(path));
			String line = bReader.readLine();
			while(line != null && !line.isEmpty()) {
				Property p = null;
				String[] res = line.split(SEPERATOR);
				if(res.length == 1) {
					p = new Property(line);
				} else {
					String[] subClassHierachyDomain = res[0].split(TAB);
					String[] subClassHierachyRange = res[2].split(TAB);
					for (int i = 0; i < subClassHierachyDomain.length; i++) {
						String type = subClassHierachyDomain[i].trim();
						subClassHierachyDomain[i] = type;
					}
					for (int i = 0; i < subClassHierachyRange.length; i++) {
						String type = subClassHierachyRange[i].trim();
						subClassHierachyRange[i] = type;
					}
					System.out.println();
					p = new Property(res[1].trim(), subClassHierachyDomain, subClassHierachyRange);
				}
				if(p != null) {
					propertyList.add(p);
				}
				line = bReader.readLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return propertyList;
	}
	
	public static void main(String[] args) {
		StorageService s = null;
		s = new StorageService();
		try {
			s.initReader(new File("").getAbsolutePath() + "/results/Example/Properties/examplekb.prop",0);
		} catch (ResultOffsetException | FileNotFoundException e) {
			e.printStackTrace();
		}
		s.readPropertiesAsProperty(new File("").getAbsolutePath() + "/results/Example/Properties/examplekb.prop");
	
	}

	/**
	 * Sets up a BufferedReader to read from a file with the given path and an
	 * offset stating where to start to read.
	 * @param path to file to read from
	 * @throws ResultOffsetException 
	 * @throws FileNotFoundException 
	 */
	public void initReader(String path, int offset) throws ResultOffsetException, FileNotFoundException {
		this.bReader = new BufferedReader(new FileReader(path));
		for (int i = 0; i < offset-1; i++) {
			if(!hasNextResult()){
				throw new ResultOffsetException(i, offset);
			}
		}
	}
	
	/**
	 * Checks if there is another result in the file
	 * @return true if there is a next result in the current file otherwise it returns false 
	 * representing that the reader is at the end of the current chunk file.
	 */
	public boolean hasNextResult() {
		try {
			this.currReadResults = bReader.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if(this.currReadResults != null)
			return true;
		else 
			return false;
	}
	
	/**
	 * Reads the current file line and creates a TRule object
	 * @return
	 */
	public Rule nextRuleTSV() {
		Rule tr = null;
		if(currReadResults != null) {
			String[] results = currReadResults.split(TAB);
			ArrayList<Property> propsBody = new ArrayList<>();
			for (int i = 0; i < results.length-5; i++) {
				propsBody.add(new Property(results[i]));
			}
			int l = results.length;
			tr = new Rule(propsBody, new Property(results[l-5]), Boolean.valueOf(results[l-1]));
			tr.setCount(Integer.valueOf(results[l-4]));
			tr.setConf(Double.valueOf(results[l-3]));
			tr.setSupport(Double.valueOf(results[l-2]));
		}
		return tr;
	}
	
	/**
	 * Returns the plain string from the current reading line
	 * @return
	 */
	public String nextResult() {
		return currReadResults;
	}
	
}
